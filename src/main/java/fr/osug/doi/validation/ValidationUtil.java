/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.validation;

import fr.osug.doi.Const;
import fr.osug.doi.ProcessPipelineDoiData;
import fr.osug.doi.DoiCsvData;
import java.text.Normalizer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class ValidationUtil {

    private final static Logger logger = LoggerFactory.getLogger(ValidationUtil.class.getName());
    /** Empty String constant '' */
    public final static String STRING_EMPTY = "";
    /** String constant containing 1 space character ' ' */
    public final static String STRING_SPACE = " ";

    /** regular expression used to match characters different than alpha/numeric/_/-/. (1..n) */
    private final static Pattern PATTERN_IDENTIFIER = Pattern.compile("[^a-zA-Z0-9\\-_\\.]");
    /** regular expression used to match characters different than alpha (1..n) */
    private final static Pattern PATTERN_NON_ALPHA = Pattern.compile("[^a-zA-Z]+");

    /** regular expression used to match characters with accents */
    private final static Pattern PATTERN_ACCENT_CHARS = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

    /** RegExp expression to match white spaces (1..n) */
    private final static Pattern PATTERN_WHITE_SPACE_MULTIPLE = Pattern.compile("\\s+");
    /** simplify name cache */
    private final static Map<String, String> cacheSimpleNames = new ConcurrentHashMap<String, String>(128);

    private ValidationUtil() {
    }

    public static boolean checkIdentifier(final String id, final Object data) {
        if (id == null || id.trim().isEmpty()) {
            // Missing identifier
            logger.error("Missing ''"
                    + Const.KEY_IDENTIFIER + "'' in dataset:\n{}", data);
            return false;
        }
        final String suffix = DoiCsvData.getDoiSuffix(id);
        if (suffix.trim().isEmpty()) {
            // Missing suffix
            logger.error("Missing suffix for ''"
                    + Const.KEY_IDENTIFIER + "'' in dataset:\n{}", data);
            return false;
        }
        return true;
    }

    public static void validateIdentifier(final String id, final ProcessPipelineDoiData pipeData) {
        // Check test prefix:
        if (!isTestPrefix(id)) {
            // identifier does not starts with the test prefix
            pipeData.addError("Invalid ''"
                    + Const.KEY_IDENTIFIER + "'': must be equals to the TEST prefix: " + Const.DOI_PREFIX_TEST);
        }
        final String suffix = DoiCsvData.getDoiSuffix(id);
        if (!checkIdentifier(suffix)) {
            // Invalid suffix
            pipeData.addError("Invalid suffix for ''" + Const.KEY_IDENTIFIER + "'' = '" + suffix + "'");
        }
    }

    public static boolean isTestPrefix(final String id) {
        return id.startsWith(Const.DOI_PREFIX_TEST);
    }

    /**
     * Check the DOI identifier against the regexp
     * @param value input value
     * @return true if the DOI identifier is valid
     */
    private static boolean checkIdentifier(final String value) {
        return !PATTERN_IDENTIFIER.matcher(value).matches();
    }

    /** string utils */
    /**
     * Test if value is empty (null or no chars)
     * 
     * @param value string value
     * @return true if value is empty (null or no chars)
     */
    public static boolean isEmpty(final String value) {
        return value == null || value.length() == 0;
    }

    /**
     * Replace any non alpha numeric character by ' ' and remove accents
     * @param value input value
     * @return string value
     */
    public static String simplifyName(final String value) {
        String res = cacheSimpleNames.get(value);
        if (res == null) {
            res = cleanWhiteSpaces(replaceNonAlphaChars(removeAccents(value), STRING_SPACE));
            logger.debug("simplifyName: '{}' => '{}'", value, res);
            cacheSimpleNames.put(value, res);
        }
        return res;
    }

    /**
     * Replace non alpha numeric characters by the given replacement string
     * @param value input value
     * @param replaceBy replacement string
     * @return string value
     */
    private static String replaceNonAlphaChars(final String value, final String replaceBy) {
        return PATTERN_NON_ALPHA.matcher(value).replaceAll(replaceBy);
    }

    /**
     * Remove accents from any character i.e. remove diacritical marks
     * @param value input value
     * @return string value
     */
    private static String removeAccents(final String value) {
        // Remove accent from characters (if any) (Java 1.6)
        final String normalized = Normalizer.normalize(value, Normalizer.Form.NFD);

        return PATTERN_ACCENT_CHARS.matcher(normalized).replaceAll(STRING_EMPTY);
    }

    /**
     * Trim and remove redundant white space characters
     * @param value input value
     * @return string value
     */
    private static String cleanWhiteSpaces(final String value) {
        return isEmpty(value) ? STRING_EMPTY : replaceWhiteSpaces(value.trim(), STRING_SPACE);
    }

    /**
     * Replace white space characters (1..n) by the given replacement string
     * @param value input value
     * @param replaceBy replacement string
     * @return string value
     */
    private static String replaceWhiteSpaces(final String value, final String replaceBy) {
        return PATTERN_WHITE_SPACE_MULTIPLE.matcher(value).replaceAll(replaceBy);
    }

}
