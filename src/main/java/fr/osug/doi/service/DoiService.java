/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.service;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiPublic;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Project;
import fr.osug.doi.domain.Status;
import fr.osug.doi.repository.DoiBaseRepository;
import fr.osug.doi.repository.DoiPublicRepository;
import fr.osug.doi.repository.DoiRepository;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.repository.ProjectRepository;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing Doi.
 */
@Service
public class DoiService {

    private final static Logger logger = LoggerFactory.getLogger(DoiService.class);

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private DoiRepository doiRepository;

    @Autowired
    private DoiStagingRepository doiStagingRepository;

    @Autowired
    private DoiPublicRepository doiPublicRepository;

    @Transactional
    public Project getOrCreateProject(final String name, final Date now) {
        Project project = projectRepository.findOneByName(name);

        if (project == null) {
            project = new Project();
            project.setName(name);

            project.setCreateDate(now);
            project.setUpdateDate(now);

            logger.debug("saving: {}", project);

            projectRepository.save(project);
        }
        return project;
    }

    @Transactional
    public void updateProjectDate(final String name, final Date now) {
        Project project = projectRepository.findOneByName(name);

        if (project == null) {
            throw new IllegalArgumentException("Missing project with name: " + name);
        }

        project.setUpdateDate(now);

        logger.debug("saving: {}", project);

        projectRepository.save(project);
    }

    @Transactional
    public void removeOrphanProject(final String name) {
        // Check no DOI references this project:
        final List<Doi> dois = doiRepository.findByProject(name);

        if (!dois.isEmpty()) {
            throw new IllegalArgumentException("Project [" + name + "] is not empty - can not remove !");
        }

        Project project = projectRepository.findOneByName(name);

        if (project == null) {
            throw new IllegalArgumentException("Missing project with name: " + name);
        }

        logger.debug("deleting: {}", project);

        projectRepository.delete(project);
    }

    @Transactional(readOnly = true)
    public Doi getDoi(final String identifier) {
        return doiRepository.findOneByIdentifier(identifier);
    }

    @Transactional
    public Doi createOrUpdateDoi(final Project project, final String identifier,
                                 final String description) {

        Doi doi = doiRepository.findOneByIdentifier(identifier);

        if (doi == null) {
            doi = new Doi();
            doi.setProject(project);
            doi.setIdentifier(identifier);
            doi.setStatus(Status.STAGING);
        } else if (doi.getStatus() != Status.PUBLIC) {
            if (!doi.getProject().equals(project)) {
                logger.info("modified project: {} for {}", project.getName(), doi.getIdentifier());
                doi.setProject(project);
            }
        }

        doi.setDescription(description);

        logger.debug("saving: {}", doi);

        doiRepository.save(doi);

        return doi;
    }

    @Transactional
    public Doi updateDoiStatus(final String identifier,
                               final Status status) {

        Doi doi = doiRepository.findOneByIdentifier(identifier);

        if (doi == null) {
            throw new IllegalStateException("Missing DOI[" + identifier + "] !");
        }

        // only update status (PUBLIC > STAGING)
        if (doi.getStatus() != Status.PUBLIC) {
            doi.setStatus(status);
        }
        logger.debug("saving: {}", doi);

        doiRepository.save(doi);

        return doi;
    }

    @Transactional
    public boolean createOrUpdateDoiStaging(final Project project, final String identifier,
                                            final String description,
                                            final String metadataMd5, final boolean valid,
                                            final String log, final Date now) {

        final Doi doi = createOrUpdateDoi(project, identifier, description);

        DoiStaging doiStaging = doiStagingRepository.findOneByDoi(doi);

        boolean changed = false;

        if (doiStaging == null) {
            doiStaging = new DoiStaging();
            doiStaging.setDoi(doi);

            doiStaging.setCreateDate(now);
            changed = true;
        }

        if (!metadataMd5.equals(doiStaging.getMetadataMd5())) {
            logger.info("{} md5 modified: {}", doi.getIdentifier(), metadataMd5);
            doiStaging.setMetadataMd5(metadataMd5);
            changed = true;
        }
        if (valid != doiStaging.isValid()) {
            doiStaging.setValid(valid);
            changed = true;
        }
        if (!log.equals(doiStaging.getLog())) {
            doiStaging.setLog(log);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiStaging.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiStaging);

        doiStagingRepository.save(doiStaging);

        return changed;
    }

    @Transactional
    public boolean updateStagingDoiUrls(final String identifier,
                                        final String dataAccessUrl, final String dataAccessAltUrl, 
                                        final String landingExtUrl,
                                        final boolean urlValid, final String urlLog,
                                        final Date now) {

        final DoiStaging doiStaging = doiStagingRepository.findOneByDoiIdentifier(identifier);

        if (doiStaging == null) {
            throw new IllegalStateException("Missing DoiStaging[" + identifier + "] !");
        }

        boolean changed = false;

        final String oldDataAccessUrl = doiStaging.getDataAccessUrl();
        if ((dataAccessUrl != null && !dataAccessUrl.equals(oldDataAccessUrl))
                || (oldDataAccessUrl != null && !oldDataAccessUrl.equals(dataAccessUrl))) {
            doiStaging.setDataAccessUrl(dataAccessUrl);
            changed = true;
        }
        final String oldDataAccessAltUrl = doiStaging.getDataAccessAltUrl();
        if ((dataAccessAltUrl != null && !dataAccessAltUrl.equals(oldDataAccessAltUrl))
                || (oldDataAccessAltUrl != null && !oldDataAccessUrl.equals(dataAccessUrl))) {
            doiStaging.setDataAccessAltUrl(dataAccessAltUrl);
            changed = true;
        }
        final String oldLandingExtUrl = doiStaging.getLandingExtUrl();
        if ((landingExtUrl != null && !landingExtUrl.equals(oldLandingExtUrl))
                || (oldLandingExtUrl != null && !oldLandingExtUrl.equals(landingExtUrl))) {
            doiStaging.setLandingExtUrl(landingExtUrl);
            changed = true;
        }
        if (urlValid != doiStaging.isUrlValid()) {
            doiStaging.setUrlValid(urlValid);
            changed = true;
        }
        if (!urlLog.equals(doiStaging.getUrlLog())) {
            doiStaging.setUrlLog(urlLog);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiStaging.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiStaging);

        doiStagingRepository.save(doiStaging);

        return changed;
    }

    @Transactional
    public boolean updateStagingDoiLocUrl(final String identifier,
                                          final String landingLocUrl, final Date now) {

        final DoiStaging doiStaging = doiStagingRepository.findOneByDoiIdentifier(identifier);

        if (doiStaging == null) {
            throw new IllegalStateException("Missing DoiStaging[" + identifier + "] !");
        }

        boolean changed = false;

        if (!landingLocUrl.equals(doiStaging.getLandingLocUrl())) {
            doiStaging.setLandingLocUrl(landingLocUrl);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiStaging.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiStaging);

        doiStagingRepository.save(doiStaging);

        return changed;
    }

    @Transactional
    public boolean updateStagingDoiDatacite(final String identifier,
                                            final String dataciteMetadataMd5,
                                            final String dataciteUrl, final Date now) {

        final DoiStaging doiStaging = doiStagingRepository.findOneByDoiIdentifier(identifier);

        if (doiStaging == null) {
            throw new IllegalStateException("Missing DoiStaging[" + identifier + "] !");
        }

        boolean changed = false;

        if (!dataciteMetadataMd5.equals(doiStaging.getDataciteMetadataMd5())) {
            doiStaging.setDataciteMetadataMd5(dataciteMetadataMd5);
            changed = true;
        }
        if (!dataciteUrl.equals(doiStaging.getDataciteUrl())) {
            doiStaging.setDataciteUrl(dataciteUrl);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiStaging.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiStaging);

        doiStagingRepository.save(doiStaging);

        return changed;
    }

    @Transactional
    public void removeStagingDoiDatacite(final String identifier) {

        final DoiStaging doiStaging = doiStagingRepository.findOneByDoiIdentifier(identifier);

        if (doiStaging == null) {
            throw new IllegalStateException("Missing DoiStaging[" + identifier + "] !");
        }
        if (doiStaging.getDoi().getStatus() == Status.PUBLIC) {
            throw new IllegalStateException("Can not remove DoiStaging[" + identifier + "] as it is public !");
        }

        logger.debug("deleting: {}", doiStaging);

        doiStagingRepository.delete(doiStaging);

        logger.debug("deleting: {}", doiStaging.getDoi());
        doiRepository.delete(doiStaging.getDoi());
    }

    @Transactional
    public DoiPublic createOrUpdateDoiPublic(final DoiStaging doiStaging,
                                             final String metadataMd5, final Date now) {

        // Set Status to PUBLIC:
        final Doi doi = updateDoiStatus(doiStaging.getDoi().getIdentifier(), Status.PUBLIC);

        DoiPublic doiPublic = doiPublicRepository.findOneByDoi(doi);

        if (doiPublic == null) {
            doiPublic = new DoiPublic();
            doiPublic.setDoi(doi);

            doiPublic.setCreateDate(now);
        }

        doiPublic.setUpdateDate(now);
        doiPublic.setMetadataMd5(metadataMd5);
        doiPublic.setDataAccessUrl(doiStaging.getDataAccessUrl());
        doiPublic.setDataAccessAltUrl(doiStaging.getDataAccessAltUrl());
        doiPublic.setLandingExtUrl(doiStaging.getLandingExtUrl());

        // TODO: handle external url checks
        doiPublic.setActiveExtUrl(true);

        logger.debug("saving: {}", doiPublic);

        doiPublicRepository.save(doiPublic);

        return doiPublic;
    }

    @Transactional
    public boolean updatePublicDoiLocUrl(final String identifier,
                                         final String landingLocUrl, final Date now) {

        final DoiPublic doiPublic = doiPublicRepository.findOneByDoiIdentifier(identifier);

        if (doiPublic == null) {
            throw new IllegalStateException("Missing DoiPublic[" + identifier + "] !");
        }

        boolean changed = false;

        if (!landingLocUrl.equals(doiPublic.getLandingLocUrl())) {
            doiPublic.setLandingLocUrl(landingLocUrl);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiPublic.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiPublic);

        doiPublicRepository.save(doiPublic);

        return changed;
    }

    @Transactional
    public boolean updatePublicDoiDatacite(final String identifier,
                                           final String dataciteMetadataMd5,
                                           final String dataciteUrl, final Date now) {

        final DoiPublic doiPublic = doiPublicRepository.findOneByDoiIdentifier(identifier);

        if (doiPublic == null) {
            throw new IllegalStateException("Missing DoiPublic[" + identifier + "] !");
        }

        boolean changed = false;

        if (!dataciteMetadataMd5.equals(doiPublic.getDataciteMetadataMd5())) {
            doiPublic.setDataciteMetadataMd5(dataciteMetadataMd5);
            changed = true;
        }
        if (!dataciteUrl.equals(doiPublic.getDataciteUrl())) {
            doiPublic.setDataciteUrl(dataciteUrl);
            changed = true;
        }
        // update date only if changed:
        if (changed) {
            doiPublic.setUpdateDate(now);
        }
        logger.debug("saving: {}", doiPublic);

        doiPublicRepository.save(doiPublic);

        return changed;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public DoiRepository getDoiRepository() {
        return doiRepository;
    }

    public DoiBaseRepository<?> getDoiCommonRepository(final boolean isStaging) {
        return (isStaging) ? doiStagingRepository : doiPublicRepository;
    }

    public DoiPublicRepository getDoiPublicRepository() {
        return doiPublicRepository;
    }

    public DoiStagingRepository getDoiStagingRepository() {
        return doiStagingRepository;
    }

    public static String convertPattern(final String doiPattern) {
        String pattern;

        if ("*".equals(doiPattern)) {
            pattern = "";
        } else {
            // escape '%' and '_'
            pattern = doiPattern.replaceAll("_", "\\_");
            pattern = pattern.replaceAll("%", "\\%");
        }

        // '<pattern>%' 
        return pattern + "%";
    }

}
