/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.service;

import fr.osug.doi.DataciteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestClientException;

/**
 * Basic HTTP GET for remote URL
 */
@Service
public class UrlChecker {

    private final static Logger logger = LoggerFactory.getLogger(DataciteConfig.class.getName());

    public final static int TIMEOUT_CONNECT = 10000;
    public final static int TIMEOUT_READ = 30000;

    private final RestTemplate restTemplate;

    public UrlChecker(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder
                .setConnectTimeout(TIMEOUT_CONNECT)
                .setReadTimeout(TIMEOUT_READ)
                .build();
    }

    /**
        GET resource

        Response statuses
        200 OK - operation successful
    
        @param url resource to load
        @param headOnly true to only retrieve content-type
        @return resource as string or Content-Type header if headOnly is true
     */
    public String getResource(final String url, final boolean headOnly) {
        // Note: url must not be encoded (%)
        if (url.contains("%")) {
            logger.info("getResource: url is encoded (may cause problem with HTTP request) : {}", url);
        } else {
            logger.debug("getResource: {}", url);
        }
        String response = null;
        if (headOnly) {
            try {
                final HttpHeaders headers = this.restTemplate.headForHeaders(url);

                if (headers != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("headers: {}", headers.values());
                    }
                    response = headers.getContentType().toString();
                }
            } catch (RestClientException rce) {
                logger.warn("getResource({}): failed: {}", url, rce.getMessage());
            }
        }
        // if not headOnly or head failed:
        if (response == null) {
            try {
                response = this.restTemplate.getForObject(url, String.class);
            } catch (RestClientException rce) {
                logger.warn("getResource({}): failed: {}", url, rce.getMessage());
            }
        }
        logger.debug("getResource({}):\n{}", url, response);
        return response;
    }
}
