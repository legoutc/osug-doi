/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.service;

import fr.osug.doi.Const;
import fr.osug.doi.DataciteConfig;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

/**
 *
 */
public class DataciteClient {

    private final static Logger logger = LoggerFactory.getLogger(DataciteConfig.class.getName());

    public final static int TIMEOUT_CONNECT = 10000;
    public final static int TIMEOUT_READ = 30000;

    public final static String BASE_URL = "https://mds.datacite.org";
    /* specific content-types */
    public final static String PLAIN_UTF8 = "text/plain;charset=UTF-8";
    public final static String XML_UTF8 = "application/xml;charset=UTF-8";

    // members:
    private final RestTemplate restTemplate;
    private final boolean restrictToTestPrefix;

    public DataciteClient(RestTemplateBuilder restTemplateBuilder, boolean enablePublic) {
        this.restTemplate = restTemplateBuilder.rootUri(BASE_URL)
                .setConnectTimeout(TIMEOUT_CONNECT)
                .setReadTimeout(TIMEOUT_READ)
                .build();
        this.restrictToTestPrefix = !enablePublic;
    }

    /**
    GET (list all DOIs)
    URI: https://mds.datacite.org/doi

    This request returns a list of all DOIs for the requesting datacentre. There is no guaranteed order.

    Response body
    If response status is 200: list of DOIs, one DOI per line; empty for 204

    Response statuses
    200 OK - operation successful
    204 No Content - no DOIs founds

    @return list of DOI
     */
    public List<String> getDois() {
        List<String> response = null;
        try {
            final String text = this.restTemplate.getForObject("/doi", String.class);
            if (text != null) {
                response = Arrays.asList(text.split("\n"));
            }
        } catch (RestClientException rce) {
            logger.error("getDois: failed", rce);
        }
        logger.debug("getDois:\n{}", response);
        return response;
    }

    /**
    GET (a specific DOI)
    URI: https://mds.datacite.org/doi/{doi} where {doi} is a specific DOI.

    This request returns an URL associated with a given DOI.

    Response body
    If response status is 200: URL representing a dataset; empty for 204; otherwise short explanation for non-200 status

    Response statuses
    200 OK - operation successful
    204 No Content - DOI is known to MDS, but is not minted (or not resolvable e.g. due to handle's latency)
    404 Not Found - DOI does not exist in our database

    @param doi
    @return URL associated with a given DOI
     */
    public String getDoi(final String doi) {
        String url = null;
        try {
            url = this.restTemplate.getForObject("/doi/{doi}", String.class, doi);
        } catch (HttpClientErrorException hcee) {
            if (hcee.getStatusCode() == HttpStatus.NOT_FOUND) {
                logger.debug("getDoi[{}]: failed", doi, hcee);
            } else {
                logger.error("getDoi[{}]: failed", doi, hcee);
            }
        } catch (RestClientException rce) {
            logger.error("getDoi[{}]: failed", doi, rce);
        }
        logger.debug("getDoi[{}]: {}", doi, url);
        return url;
    }

    /**
    Publish a DOI
    
    @param doi
    @param url
    @param metadata
    @return true if succesful; false otherwise
     */
    public boolean publishDoi(final String doi, final String url, final String metadata) {
        if (restrictToTestPrefix && !doi.startsWith(Const.DOI_PREFIX_TEST)) {
            logger.info("Skipping DOI[{}]: only Test prefix is allowed.", doi);
            return false;
        }

        // 1. Metadata (first):
        final String oldMetadata = getMetadataForDoi(doi);

        if (oldMetadata == null || !oldMetadata.equals(metadata)) {
            if (!setMetadataForDoi(doi, metadata)) {
                return false;
            }
        }

        // 2. create / update the URL associated with the DOI:
        final String oldUrl = getDoi(doi);

        if (oldUrl == null || !oldUrl.equals(url)) {
            return setDoi(doi, url);
        }

        return true;
    }

    /**
    POST
    URI: https://mds.datacite.org/doi

    POST will mint new DOI if specified DOI doesn't exist. 
    This method will attempt to update URL if you specify existing DOI. 
    A new record in Datasets will be created.

    Request headers
    Content-Type:text/plain;charset=UTF-8

    Request body
    doi={doi}
    url={url}
    where {doi} and {url} have to be replaced by your DOI and URL, UFT-8 encoded.

    Response body
    short explanation of status code e.g. CREATED, HANDLE_ALREADY_EXISTS etc

    Response statuses
    201 Created - operation successful
    400 Bad Request - request body must be exactly two lines: DOI and URL; wrong domain, wrong prefix
    401 Unauthorized - no login
    403 Forbidden - login problem, quota exceeded
    412 Precondition failed - metadata must be uploaded first
    500 Internal Server Error - server internal error, try later and if problem persists please contact us

    @param doi
    @param url
    @return true if succesful; false otherwise
     */
    public boolean setDoi(final String doi, final String url) {
        if (restrictToTestPrefix && !doi.startsWith(Const.DOI_PREFIX_TEST)) {
            logger.info("Skipping DOI[{}]: only Test prefix is allowed.", doi);
            return false;
        }
        logger.debug("setDoi: doi: {} url: {}", doi, url);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, PLAIN_UTF8);

        final String parameters = "doi=" + doi + "\nurl=" + url;
        logger.debug("parameters: {}", parameters);

        final HttpEntity<String> request = new HttpEntity<String>(parameters, headers);

        boolean result = false;
        try {
            final String response = this.restTemplate.postForObject("/doi", request, String.class);
            logger.debug("setDoi[{}]: {}", doi, response);
            result = true;
        } catch (RestClientException rce) {
            logger.error("setDoi[{}]: failed", doi, rce);
        }
        return result;
    }

    /* Metadata API */
    /**
    GET
    URI: https://mds.datacite.org/metadata/{doi} where {doi} is a specific DOI.

    This request returns the most recent version of metadata associated with a given DOI.

    Request headers
    Accept:application/xml

    Response headers
    Content-Type:application/xml

    Response body
    If response status is 200: XML representing a dataset, otherwise short explanation for non-200 status

    Response statuses
    200 OK - operation successful
    401 Unauthorized - no login
    403 Forbidden - login problem or dataset belongs to another party
    404 Not Found - DOI does not exist in our database
    410 Gone - the requested dataset was marked inactive (using DELETE method)
    500 Internal Server Error - server internal error, try later and if problem persists please contact us
    
    @param doi
    @return 
     */
    public String getMetadataForDoi(final String doi) {
        String metadata = null;
        try {
            metadata = this.restTemplate.getForObject("/metadata/{doi}", String.class, doi);
        } catch (HttpClientErrorException hcee) {
            if (hcee.getStatusCode() == HttpStatus.NOT_FOUND) {
                logger.debug("getMetadataForDoi[{}]: failed", doi, hcee);
            } else {
                logger.error("getMetadataForDoi[{}]: failed", doi, hcee);
            }
        } catch (RestClientException rce) {
            logger.error("getMetadataForDoi[{}]: failed", doi, rce);
        }
        logger.debug("getMetadataForDoi[{}]: \n{}", doi, metadata);
        return metadata;
    }

    /**
    POST
    URI: https://mds.datacite.org/metadata

    This request stores new version of metadata. The request body must contain valid XML.

    Request headers
    Content-Type:application/xml;charset=UTF-8

    Request body
    UFT-8 encoded metadata

    Response body
    short explanation of status code e.g. CREATED, HANDLE_ALREADY_EXISTS etc

    Response headers
    Location - URL of the newly stored metadata

    Response statuses
    201 Created - operation successful
    400 Bad Request - invalid XML, wrong prefix
    401 Unauthorized - no login
    403 Forbidden - login problem, quota exceeded
    500 Internal Server Error - server internal error, try later and if problem persists please contact us
    
    @param doi
    @param metadata
    @return true if succesful; false otherwise
     */
    public boolean setMetadataForDoi(final String doi, final String metadata) {
        if (restrictToTestPrefix && !doi.startsWith(Const.DOI_PREFIX_TEST)) {
            logger.info("Skipping DOI[{}]: only Test prefix is allowed.", doi);
            return false;
        }
        logger.debug("setMetadataForDoi: metadata: {}", metadata);

        final HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, XML_UTF8);

        final HttpEntity<String> request = new HttpEntity<String>(metadata, headers);

        boolean result = false;
        try {
            final String response = this.restTemplate.postForObject("/metadata", request, String.class);
            logger.debug("setMetadataForDoi: {}", response);
            result = true;
        } catch (RestClientException rce) {
            logger.error("setMetadataForDoi[{}]: failed", doi, rce);
        }
        return result;
    }
}
