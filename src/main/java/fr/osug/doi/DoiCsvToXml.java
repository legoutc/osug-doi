/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class DoiCsvToXml {

    private final static Logger logger = LoggerFactory.getLogger(DoiCsvToXml.class.getName());

    DoiCsvToXml() {
        // private
    }

    public static void write(final DoiCsvData data, final StringBuilder sb) {
        if (!data.isEmpty()) {
            // split rows:
            final List<String[]> rows = data.getRows();

            sb.setLength(0);
            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            sb.append("<rows>\n");

            for (int i = 0, size = rows.size(); i < size; i++) {
                final String[] cols = rows.get(i);
                if (cols != null) {
                    if (cols.length >= 2) {
                        // convert rows to xml entry
                        final String key = cols[0];
                        final String val = cols[1];

                        if (key.isEmpty() || val.isEmpty()) {
                            continue;
                        }

                        final String[] keyItems = key.split(":");
                        final int keyLen = keyItems.length;

                        if (keyLen != 0) {
                            sb.append("<property key=\"").append(keyItems[0]).append("\"");

                            for (int j = 1; j < keyLen; j++) {
                                sb.append(" attr").append(j).append("=\"").append(keyItems[j]).append("\"");
                            }
                            sb.append('>');
                            xmlEscapeText(val, sb);

                            // extra columns serialized as extra attributes:
                            if (cols.length > 2) {
                                if ((cols.length % 2) == 1) {
                                    logger.warn("Even column count: {}):\n{}", cols.length, Arrays.toString(cols));
                                }
                                final int end = (cols.length >> 1) << 1;
                                for (int j = 2; j < end; j += 2) {
                                    final String akey = cols[j];
                                    final String aval = cols[j + 1];

                                    if (akey.isEmpty() || aval.isEmpty()) {
                                        continue;
                                    }

                                    sb.append("<attribute key=\"").append(akey).append("\">");
                                    xmlEscapeText(aval, sb);
                                    sb.append("</attribute>");
                                }
                            }

                            sb.append("</property>\n");
                        }
                    }
                }
            }
            sb.append("</rows>\n\n");
        }
    }

    public static void xmlEscapeText(final String value, final StringBuilder sb) {
        if (value != null) {
            for (int i = 0, len = value.length(); i < len; i++) {
                final char c = value.charAt(i);
                switch (c) {
                    case '<':
                        sb.append("&lt;");
                        break;
                    case '>':
                        sb.append("&gt;");
                        break;
                    case '&':
                        sb.append("&amp;");
                        break;
                    /*                    
                case '\"':
                    sb.append("&quot;");
                    break;
                case '\'':
                    sb.append("&apos;");
                    break;
                     */
                    default:
                        // use UTF-8 support:
                        sb.append(c);
                }
            }
        }
    }
}
