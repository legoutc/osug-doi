/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 *
 */
public final class NameEntry {

    public static final Comparator<NameEntry> COMPARATOR = new Comparator<NameEntry>() {
        @Override
        public int compare(final NameEntry e1, final NameEntry e2) {
            int res = e1.getName().compareToIgnoreCase(e2.getName());
            if (res == 0) {
                final int len1 = (e1.getAttributes() != null) ? e1.getAttributes().size() : 0;
                final int len2 = (e2.getAttributes() != null) ? e2.getAttributes().size() : 0;
                res = Integer.compare(len1, len2);
            }
            return res;
        }
    };

    public static NameEntry fromCsv(final String[] cols) {
        final String name = cols[1];
        final NameEntry entry;
        if (cols.length == 2) {
            entry = new NameEntry(name, null);
        } else {
            final Map<String, String> attributes = new HashMap<String, String>(4);
            for (int i = 2; i < cols.length; i += 2) {
                attributes.put(cols[i], cols[i + 1]);
            }
            entry = new NameEntry(name, attributes);
        }
        return entry;
    }

    public static String[] toCsv(final NameEntry entry) {
        if (!entry.hasAttributes()) {
            return new String[]{null, entry.getName()};
        }
        final Map<String, String> attributes = entry.getAttributes();
        final String[] cols = new String[(1 + attributes.size()) * 2];
        cols[1] = entry.getName();
        int i = 2;
        for (Entry<String, String> e : attributes.entrySet()) {
            cols[i] = e.getKey();
            cols[i + 1] = e.getValue();
            i += 2;
        }
        return cols;
    }

    // members:
    private final String name;
    private final Map<String, String> attributes;

    public NameEntry(final String name, final Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    public String getName() {
        return name;
    }

    public boolean hasAttributes() {
        return attributes != null;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String key) {
        String val = attributes.get(key);
        return (val != null) ? val : "";
    }

    @Override
    public String toString() {
        return "{name=" + name + ", attributes=" + attributes + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        hash = 29 * hash + Objects.hashCode(this.attributes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NameEntry other = (NameEntry) obj;
        if (!Objects.equals(this.name, other.getName())) {
            return false;
        }
        if (!Objects.equals(this.attributes, other.getAttributes())) {
            return false;
        }
        return true;
    }

}
