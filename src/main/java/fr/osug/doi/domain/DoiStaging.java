/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * A Doi in the staging phase
 */
@Entity
@Table(name = "doi_staging")
public class DoiStaging extends DoiCommon {

    private static final long serialVersionUID = 1L;

    @Column(name = "valid")
    private boolean valid;

    @Column(name = "log")
    private String log;

    @Column(name = "url_valid")
    private boolean urlValid;

    @Column(name = "url_log")
    private String urlLog;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public boolean isUrlValid() {
        return urlValid;
    }

    public void setUrlValid(boolean urlValid) {
        this.urlValid = urlValid;
    }

    public String getUrlLog() {
        return urlLog;
    }

    public void setUrlLog(String urlLog) {
        this.urlLog = urlLog;
    }

    public boolean isCompletelyValid() {
        return isValid() && isUrlValid();
    }

    @Override
    public String toString() {
        return "DoiStaging{"
                + super.toString()
                + ", valid='" + valid + "'"
                + ", log=[\n" + log + "\n]"
                + ", urlValid='" + urlValid + "'"
                + ", urlLog=[\n" + urlLog + "\n]"
                + '}';
    }
}
