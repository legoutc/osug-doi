/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

/**
 *
 */
public interface Const {

    public final static String SCHEMA_VERSION_3_1 = "3.1";
    public final static String SCHEMA_VERSION_4_1 = "4.1";

    public final static float F_SCHEMA_VERSION_3_1 = Float.valueOf(SCHEMA_VERSION_3_1);
    public final static float F_SCHEMA_VERSION_4_1 = Float.valueOf(SCHEMA_VERSION_4_1);

    /** datacite schema version '3.1' or '4.1' */
    public final static String SCHEMA_VERSION = SCHEMA_VERSION_4_1;
    public final static float F_SCHEMA_VERSION = Float.valueOf(SCHEMA_VERSION);

    public final static String DOI_PREFIX_TEST = "10.5072";

    public final static char SEPARATOR = ';';
    public final static char COMMENT = '#';

    public final static String FILE_EXT_CSV = ".csv";
    public final static String FILE_EXT_XML = ".xml";

    public static final String KEY_IDENTIFIER = "identifier:DOI";
    public static final String KEY_GEO_LOCATION_PLACE = "geoLocationPlace";
    public static final String KEY_TITLE = "title";

    public static final String KEY_CREATOR_NAME = "creatorName";
    public static final String KEY_CONTRIBUTOR_NAME = "contributorName";
    public static final String KEY_CONTRIBUTOR_FUNDER_NAME = "contributorName:Funder";
    public static final String KEY_FUNDER_NAME = "funderName";

    public static final String KEY_REL_ID_START = "relatedIdentifier:";
    public static final String KEY_REL_ID_DOI = ":DOI";

    public static final String[] KEY_ORDER = new String[]{
        //# 1 [identifier]
        KEY_IDENTIFIER,
        //# 2 [creators]
        KEY_CREATOR_NAME,
        // # v4.1
        "creatorName:Organizational",
        "creatorName:Personal",
        //# 3 [titles]
        KEY_TITLE,
        "title:AlternativeTitle",
        "title:Subtitle",
        "title:TranslatedTitle",
        "title:Other",
        //# 4 [publisher]
        "publisher",
        // # 5 [publicationYear]
        "publicationYear",
        //# 6 [subjects]
        "subject",
        "subject:main",
        "subject:var",
        //# 7 [contributors]
        "contributorName:ContactPerson",
        "contributorName:DataCollector",
        "contributorName:DataCurator",
        "contributorName:DataManager",
        "contributorName:Distributor",
        "contributorName:Editor",
        // Funder is deprecated in v4.0
        KEY_CONTRIBUTOR_FUNDER_NAME,
        "contributorName:HostingInstitution",
        "contributorName:Other",
        "contributorName:Producer",
        "contributorName:ProjectLeader",
        "contributorName:ProjectManager",
        "contributorName:ProjectMember",
        "contributorName:RegistrationAgency",
        "contributorName:RegistrationAuthority",
        "contributorName:RelatedPerson",
        "contributorName:ResearchGroup",
        "contributorName:RightsHolder",
        "contributorName:Researcher",
        "contributorName:Sponsor",
        "contributorName:Supervisor",
        "contributorName:WorkPackageLeader",
        //# 8 [dates]
        "date:Accepted",
        "date:Available",
        "date:Collected",
        "date:Copyrighted",
        "date:Created",
        "date:Issued",
        "date:Other",
        "date:Submitted",
        "date:Updated",
        "date:Valid",
        //# 9 [language]
        "language",
        //# 10 [resourceType] (PARTIAL)
        "resourceType:Dataset",
        "resourceType:DataPaper",
        "resourceType:Service",
        "resourceType:Software",
        "resourceType:Other",
        //# 11 [alternateIdentifiers] (IGNORED)
        "alternateIdentifier:URL",
        "alternateIdentifier:DOI",
        //# 12 [relatedIdentifiers]
        // URL:
        "relatedIdentifier:Cites:URL",
        "relatedIdentifier:References:URL",
        "relatedIdentifier:HasPart:URL",
        // DOI:
        "relatedIdentifier:IsCitedBy:DOI",
        "relatedIdentifier:Cites:DOI",
        "relatedIdentifier:IsSupplementTo:DOI",
        "relatedIdentifier:IsSupplementedBy:DOI",
        "relatedIdentifier:IsContinuedBy:DOI",
        "relatedIdentifier:Continues:DOI",
        "relatedIdentifier:IsNewVersionOf:DOI",
        "relatedIdentifier:IsPreviousVersionOf:DOI",
        "relatedIdentifier:IsPartOf:DOI",
        "relatedIdentifier:HasPart:DOI",
        "relatedIdentifier:IsReferencedBy:DOI",
        "relatedIdentifier:References:DOI",
        "relatedIdentifier:IsDocumentedBy:DOI",
        "relatedIdentifier:Documents:DOI",
        "relatedIdentifier:IsCompiledBy:DOI",
        "relatedIdentifier:Compiles:DOI",
        "relatedIdentifier:IsVariantFormOf:DOI",
        "relatedIdentifier:IsOriginalFormOf:DOI",
        "relatedIdentifier:IsIdenticalTo:DOI",
        "relatedIdentifier:HasMetadata:DOI",
        "relatedIdentifier:IsMetadataFor:DOI",
        "relatedIdentifier:Reviews:DOI",
        "relatedIdentifier:IsReviewedBy:DOI",
        "relatedIdentifier:IsDerivedFrom:DOI",
        "relatedIdentifier:IsSourceOf:DOI",
        //# 13 [sizes]
        "size",
        //# 14 [formats]
        "format",
        //# 15 [version]
        "version",
        //# 16 [rightsList]
        "rights",
        //# 17 [descriptions]
        "description",
        "description:Abstract",
        "description:Methods",
        "description:SeriesInformation",
        "description:TechnicalInfo",
        "description:TableOfContents",
        "description:Other",
        //# 18 [geoLocations]
        KEY_GEO_LOCATION_PLACE,
        "geoLocationPoint",
        "geoLocationBox",
        //# 19 [funder]
        KEY_FUNDER_NAME
    };
    public static final String[] KEY_ATTRS_NAMES = new String[]{
        // creator / contributor attributes:
        // nameIdentifier variants:
        //        "nameIdentifier:AUTHORCLAIM",
        //        "nameIdentifier:ISNI",
        "nameIdentifier:ORCID",
        //        "nameIdentifier:RESEARCHERID",
        //        "nameIdentifier:VIAF",
        //        "nameIdentifier:URL",
        // affiliation:
        "affiliation"
        //        givenName
        //        familyName
    };    
    public static final String[] KEY_ATTRS_RIGHTS = new String[]{
        // rights attribute:
        "rightsURI"
    };    
    public static final String[] KEY_ATTRS_FUNDER = new String[]{
        // funder attributes:
        //        funderIdentifier
        "awardNumber",
        "awardTitle"
    };

    public static final String[] KEY_IGNORE = new String[]{
        "Préfixe enregistrement doi",
        "Prénom Nom",
        "Attention : uniquement Prénom Nom"
    };
}
