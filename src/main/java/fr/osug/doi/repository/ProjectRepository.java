/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.repository;

import fr.osug.doi.domain.Project;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the Project entity.
 */
@Repository
@Transactional
public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Transactional(readOnly = true)
    Project findOneByName(String name);
    
    @Transactional(readOnly = true)
    List<Project> findAllByOrderByNameAsc();

    @Transactional(readOnly = true)
    @Query("select p.name from Project p order by p.name asc")
    List<String> findAllNamesOrderByNameAsc();
    
}
