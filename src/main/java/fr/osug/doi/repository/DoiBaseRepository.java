/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi.repository;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiCommon;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Spring Data JPA repository for the DoiCommon classes.
 */
@NoRepositoryBean
public interface DoiBaseRepository<T extends DoiCommon> extends Repository<T, Long> {

    @Transactional(readOnly = true)
    T findOneByDoi(Doi doi);

    @Transactional(readOnly = true)
    @Query("select ds from #{#entityName} ds join ds.doi d where (d.identifier = :identifier)")
    T findOneByDoiIdentifier(@Param("identifier") String identifier);

    @Transactional(readOnly = true)
    @Query("select ds from #{#entityName} ds join ds.doi d join d.project p where (p.name = :projectName) order by d.identifier asc")
    List<T> findByProject(@Param("projectName") String projectName);

    @Transactional(readOnly = true)
    @Query("select ds from #{#entityName} ds join ds.doi d join d.project p where (p.name = :projectName) and  (d.identifier like :pattern) order by d.identifier asc")
    List<T> findByProjectAndPattern(@Param("projectName") String projectName, @Param("pattern") String pattern);

    @Transactional(readOnly = true)
    @Query("select count(ds) from #{#entityName} ds join ds.doi d join d.project p where (p.name = :projectName)")
    Long countByProject(@Param("projectName") String projectName);

}
