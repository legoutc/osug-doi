/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.DoiCommon;
import fr.osug.doi.repository.DoiBaseRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class ProcessUrlPipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(ProcessUrlPipeline.class.getName());

    private final static int DOC_OK = 0;
    private final static int DOC_EMPTY = 1;
    private final static int DOC_INVALID = 2;

    // members:
    private final Map<String, Integer> checkUrlCache = new HashMap<String, Integer>(64);

    public ProcessUrlPipeline(final PipelineCommonData pipeData,
                              final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
    }

    @Override
    public void doExecute() throws IOException {
        for (ProjectConfig projectConfig : pipeData.getProjectConfigs()) {
            processUrls(projectConfig);
        }
    }

    private void processUrls(final ProjectConfig projectConfig) throws IOException {
        logger.info("processUrls ...");

        // always update urls in staging:
        final boolean isStaging = true;

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();
        final DoiBaseRepository<?> dbr = doiService.getDoiCommonRepository(isStaging);

        final File dataAccessFileLocation = new File(projectConfig.getProjectConf(), ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS);
        final File dataAccessFile = FileUtils.getFile(dataAccessFileLocation);
        if (dataAccessFile == null) {
            logger.warn("Missing dataAccessFile: {}", dataAccessFileLocation);
        }

        final String defaultDataAccessUrl = projectConfig.getProperty(ProjectConfig.CONF_KEY_DATA_ACCESS_URL);
        if (defaultDataAccessUrl == null) {
            logger.warn("Missing default data access url");
        }
        // optional alternative url:
        final String defaultDataAccessAltUrl = projectConfig.getProperty(ProjectConfig.CONF_KEY_DATA_ACCESS_ALT_URL);
        if (defaultDataAccessAltUrl != null) {
            final File dataAccessAltFileLocation = new File(projectConfig.getProjectConf(), ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS_ALT);
            final File dataAccessAltFile = FileUtils.getFile(dataAccessAltFileLocation);
            if (dataAccessAltFile == null) {
                logger.warn("Missing dataAccessAltFile: {}", dataAccessAltFileLocation);
            }
        }

        final List<? extends DoiCommon> dList = dbr.findByProject(projectName);

        logger.debug("DoiCommon list for project[{}]: {}", projectName, dList);

        boolean changed = false;

        for (DoiCommon d : dList) {
            logger.debug("DoiCommon: {}", d);

            final String doiSuffix = d.getDoi().getIdentifier();
            logger.info("processing {}", doiSuffix);

            final PipelineCommonDoiData doiData = new PipelineCommonDoiData(doiSuffix);

            if (dataAccessFile == null) {
                doiData.addError("Missing data access instructions (" + ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS + ")");
            }

            // Get specific data access URL:
            String dataAccessUrl = projectConfig.getUrlDataAccess(doiSuffix);
            if (dataAccessUrl == null) {
                dataAccessUrl = defaultDataAccessUrl;
            }
            if (dataAccessUrl == null) {
                doiData.addError("Missing data access url");
            } else {
                checkUrl(doiData, dataAccessUrl, true);
                logger.debug("dataAccessUrl {}", dataAccessUrl);
            }

            // Get (optional) specific alternative data access URL:
            String dataAccessAltUrl = projectConfig.getUrlDataAccessAlt(doiSuffix);
            if (dataAccessAltUrl == null) {
                dataAccessAltUrl = defaultDataAccessAltUrl;
            }
            if (dataAccessAltUrl != null) {
                checkUrl(doiData, dataAccessAltUrl, true);
                logger.debug("dataAccessAltUrl {}", dataAccessAltUrl);
            }

            // Get external Landing page URL:
            final String landingPageExtUrl = projectConfig.getUrlLandingPage(doiSuffix);
            if (landingPageExtUrl != null) {
                checkUrl(doiData, landingPageExtUrl, false);
                logger.debug("landingPageUrl: {}", landingPageExtUrl);
            }

            changed |= doiService.updateStagingDoiUrls(doiSuffix, dataAccessUrl, dataAccessAltUrl, landingPageExtUrl,
                    doiData.isValid(), doiData.messagesToString(), now);
        }

        if (changed) {
            // Update Project:
            doiService.updateProjectDate(projectName, now);
        }

        logger.info("processUrls: done");
    }

    private void checkUrl(final PipelineCommonDoiData doiData, final String url, final boolean binary) {
        logger.debug("checkUrl: {}", url);

        int check = DOC_EMPTY;
        if (url != null) {
            final Integer cached = checkUrlCache.get(url);
            if (cached != null) {
                check = cached.intValue();
            } else {
                final String doc = doiConfig.getUrlChecker().getResource(url, binary);
                check = checkDocument(doc);
                checkUrlCache.put(url, check);
            }
        }
        logger.debug("check: {}", check);
        switch (check) {
            case DOC_EMPTY:
                doiData.addWarning("Invalid resource URL [" + url + ']');
                break;
            case DOC_INVALID:
                doiData.addWarning("Invalid page [" + url + ']');
                break;
            case DOC_OK:
            default:
        }
    }

    private static int checkDocument(final String doc) {
        if (doc == null || doc.trim().isEmpty()) {
            return DOC_EMPTY;
        } else {
            // TODO: externalize such patterns (per project ?)
            if (doc.contains("<p>No articles here</p>")) {
                return DOC_INVALID;
            }
        }
        return DOC_OK;
    }
}
