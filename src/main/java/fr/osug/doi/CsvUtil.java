/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class CsvUtil {

    private final static Logger logger = LoggerFactory.getLogger(CsvUtil.class.getName());

    private static boolean FAIL_ON_OVERWRITE = true;

    private CsvUtil() {
        // forbidden
    }

    public static CsvData read(final File csvFile) throws IOException {
        final CSVReader reader = new CSVReader(FileUtils.getTextReader(csvFile), Const.SEPARATOR);
        return new CsvData(csvFile.getAbsolutePath(), reader.readAll());
    }

    public static void write(final CsvData data, final File csvFile) throws IOException {
        if (!data.isEmpty()) {
            logger.info("Writing: {}", csvFile.getAbsolutePath());
            write(data, new FileOutputStream(csvFile));
        }
    }

    public static void write(final CsvData data, final OutputStream out) throws IOException {
        final CSVWriter writer = new CSVWriter(FileUtils.getTextWriter(out), Const.SEPARATOR);
        try {
            writer.writeAll(data.getRows(), false);
        } finally {
            writer.close();
        }
    }

    public static File[] findCSVFiles(final String dirPath) throws IOException {
        final File inputDir = FileUtils.getRequiredDirectory(dirPath).getCanonicalFile();
        return findCSVFiles(inputDir);
    }

    public static File[] findCSVFiles(final File dir) throws IOException {
        logger.debug("findCSVFiles: {}", dir);
        return CsvUtil.getSortedFiles(dir, ".*\\.csv");
    }

    public static File[] getSortedFiles(final File directory, final String regexp) {
        final File[] dataFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                return name.matches(regexp);
            }
        });

        // Sort file names:
        Arrays.sort(dataFiles);

        return dataFiles;
    }

    public static Map<String, String> loadUrlMapping(final File csvFile) throws IOException {
        Map<String, String> urlMapping = Collections.emptyMap();

        if (csvFile.exists()) {
            final CsvData data = CsvUtil.read(csvFile);
            logger.debug("CSV data: {}", data);

            final List<String[]> rows = data.getRows();

            urlMapping = new LinkedHashMap<String, String>(rows.size());

            // Get mappings [DOI|URL]
            for (String[] cols : rows) {
                if (cols == null || cols.length != 2) {
                    continue;
                }
                String key = cols[0].trim();
                if (key.isEmpty() || key.charAt(0) == Const.COMMENT) {
                    continue;
                }
                String url = cols[1].trim();
                if (url.isEmpty() || !url.startsWith("http")) {
                    continue;
                }
                try {
                    // should detect repeated keys or values ?
                    urlMapping.put(key, new URL(url).toString());
                } catch (MalformedURLException mue) {
                    logger.info("invalid URL: {}", url);
                }
            }
        }
        logger.debug("urlMapping: {}", urlMapping);
        return urlMapping;
    }
}
