/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import static fr.osug.doi.GeneratePipeline.HTML_EXT;
import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Project;
import fr.osug.doi.domain.Status;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.repository.ProjectRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class RemovePipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(RemovePipeline.class.getName());

    // member:
    public RemovePipeline(final PipelineCommonData pipeData,
                          final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
    }

    @Override
    public void doExecute() throws IOException {
        // Copy current project configs:
        final List<ProjectConfig> projectConfigs = new ArrayList<ProjectConfig>(pipeData.getProjectConfigs());

        for (ProjectConfig projectConfig : projectConfigs) {
            if (remove(projectConfig, pipeData.getDoiPattern())) {
                pipeData.removeProjectConfig(projectConfig);
                
                logger.warn("Please remove project paths:\n{}", projectConfig.dumpProjectDirs());
            }
        }
    }

    private boolean remove(final ProjectConfig projectConfig, final String doiPattern) throws IOException {
        logger.info("remove ...");

        boolean skip = false;

        final String pattern = DoiService.convertPattern(doiPattern);
        logger.debug("pattern: {}", pattern);

        final String projectName = projectConfig.getProjectName();
        logger.info("project: {}", projectName);

        final DoiService doiService = doiConfig.getDoiService();
        final ProjectRepository pr = doiService.getProjectRepository();

        final Project p = pr.findOneByName(projectName);

        if (p == null) {
            logger.warn("Unknown Project [{}]", projectName);
            skip = true;
        } else {
            final Date now = pipeData.now;

            final DoiStagingRepository dsr = doiService.getDoiStagingRepository();

            final List<DoiStaging> dsList = dsr.findByProjectAndPattern(projectName, pattern);

            logger.debug("DoiCommon list for project[{}]: {}", projectName, dsList);

            if (dsList.isEmpty()) {
                logger.info("Project {} has no DOI", projectName);
                logger.info("removing {}", projectName);
                doiService.removeOrphanProject(projectName);
                skip = true;

            } else {
                boolean changed = false;

                for (DoiStaging ds : dsList) {
                    logger.debug("DoiStaging: {}", ds);

                    final Doi doi = ds.getDoi();
                    final String doiSuffix = doi.getIdentifier();

                    if (doi.getStatus() == Status.PUBLIC) {
                        logger.warn("Ignoring {} - DOI is public", doiSuffix);
                    } else {
                        changed |= remove(projectConfig, ds);
                    }
                }
                if (changed) {
                    // Update Project:
                    doiService.updateProjectDate(projectName, now);
                }
            }
        }

        logger.info("remove: done.");
        return skip;
    }

    private boolean remove(final ProjectConfig projectConfig, final DoiStaging ds) throws IOException {
        boolean changed = false;

        final DoiService doiService = doiConfig.getDoiService();

        final String doiSuffix = ds.getDoi().getIdentifier();
        logger.info("removing {}", doiSuffix);

        final File stagingDoiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, true, doiSuffix); // staging

        if (!stagingDoiFile.exists()) {
            logger.warn("Ignoring {} - missing xml at {}", doiSuffix, stagingDoiFile);
        } else {
            changed = true;

            deleteFile(stagingDoiFile);

            // remove landing pages in staging:
            final File webDir = projectConfig.getWebStagingProjectDir();
            final File webEmbedDir = projectConfig.getWebStagingProjectEmbedDir();
            final File webXmlDir = projectConfig.getWebStagingProjectXmlDir();

            removeLandingPages(projectConfig, stagingDoiFile, webDir, webEmbedDir, webXmlDir);

            doiService.removeStagingDoiDatacite(doiSuffix);
        }
        return changed;
    }

    private void removeLandingPages(final ProjectConfig projectConfig,
                                    final File xmlFileDOI,
                                    final File webDir,
                                    final File webEmbedDir,
                                    final File webXmlDir) throws IOException {

        // Note: must be in synch with GeneratePipeline.generateLandingPages()
        final String filenameNoExt = FileUtils.getFileNameWithoutExtension(xmlFileDOI);

        // main landing page:
        // [public/staging]/PROJECT/DOI_SUFFIX.html
        File outputFile = new File(webDir, filenameNoExt + HTML_EXT);
        deleteFile(outputFile);

        if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_GENERATE_EMBEDDED)) {
            // embedded mode - full:
            // [public/staging]/PROJECT/embed/DOI_SUFFIX[.html]
            outputFile = new File(webEmbedDir, filenameNoExt + HTML_EXT);
            deleteFile(outputFile);

            // embedded mode - header:
            outputFile = new File(webEmbedDir, filenameNoExt + "-header" + HTML_EXT);
            deleteFile(outputFile);

            // embedded mode - meta:
            outputFile = new File(webEmbedDir, filenameNoExt + "-meta" + HTML_EXT);
            deleteFile(outputFile);
        }

        // Xml DOI:
        outputFile = new File(webXmlDir, xmlFileDOI.getName());
        deleteFile(outputFile);
    }

    private static void deleteFile(final File file) {
        if (file.exists()) {
            logger.info("deleting {}", file);
            file.delete();
        }
    }
}
