/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.NameEntry;
import fr.osug.util.StringBuilderWriter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 */
public final class ProcessPipelineData extends PipelineCommonData {

    private final static Set<String> NO_REFS = Collections.emptySet();

    /* members */
    /** temporary StringBuilderWriter to hold doi document */
    final StringBuilderWriter doiDoc = new StringBuilderWriter(BUFFER_SIZE);
    // References:
    private final Map<String, Set<String>> doiRefs = new LinkedHashMap<String, Set<String>>();
    /* doi data (insertion order) */
    private final Map<String, ProcessPipelineDoiData> datas = new LinkedHashMap<String, ProcessPipelineDoiData>();
    // Name references:
    private final TreeSet<NameEntry> nameRefs = new TreeSet<NameEntry>(NameEntry.COMPARATOR);

    public ProcessPipelineData(final PathConfig pathConfig) {
        super(pathConfig);
    }
    
    public void reset() {
        doiRefs.clear();
        datas.clear();
        nameRefs.clear();
    }

    public Map<String, ProcessPipelineDoiData> getDoiDatas() {
        return datas;
    }

    ProcessPipelineDoiData newDoiData(final String doiId, final String doiSuffix, final String title) {
        ProcessPipelineDoiData doiData = getDoiData(doiId);
        // ensure unicity:
        if (doiData != null) {
            doiData.addError("Duplicate detected for DOI: " + doiSuffix);
            // ignore later
            doiData = null;
        } else {
            doiData = new ProcessPipelineDoiData(doiId, doiSuffix, title);
            datas.put(doiId, doiData);
        }
        return doiData;
    }

    ProcessPipelineDoiData getDoiData(final String doiId) {
        return datas.get(doiId);
    }

    void addRefs(final String doiId, final Set<String> refs) {
        doiRefs.put(doiId, (refs != null) ? refs : NO_REFS);
    }

    boolean hasDoiId(final String doiId) {
        return doiRefs.get(doiId) != null;
    }

    Set<String> getDoiIds() {
        return doiRefs.keySet();
    }

    Set<String> getDoiRefs(final String id) {
        return doiRefs.get(id);
    }

    void addNameRef(final String[] cols) {
        nameRefs.add(NameEntry.fromCsv(cols));
    }

    public TreeSet<NameEntry> getNameRefs() {
        return nameRefs;
    }

}
