/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class PublishPipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(PublishPipeline.class.getName());

    // member:
    private List<String> publishedDois = null;

    public PublishPipeline(final PipelineCommonData pipeData,
                           final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
    }

    @Override
    public void doExecute() throws IOException {
        /*
    reécrire les identifiants DOI (identifier et relatedIdentifier) pour remplacer le préfixe de test en préfixe OSUG à l'aide de la base de données
    copier la landing page (URL publiée chez datacite): PUBLIC
    gérer les redirections ie une URL alternative pour la landing page
    publier ce DOI avec le préfixe OSUG et mettre à jour la BDD
         */
        for (ProjectConfig projectConfig : pipeData.getProjectConfigs()) {
            publish(projectConfig, pipeData.getDoiPattern());
        }
    }

    private void publish(final ProjectConfig projectConfig, final String doiPattern) throws IOException {
        logger.info("publish ...");

        final String pattern = DoiService.convertPattern(doiPattern);
        logger.debug("pattern: {}", pattern);

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();
        final DoiStagingRepository dsr = doiService.getDoiStagingRepository();

        final List<DoiStaging> dsList = dsr.findByProjectAndPattern(projectName, pattern);

        logger.debug("DoiCommon list for project[{}]: {}", projectName, dsList);

        boolean changed = false;

        for (DoiStaging ds : dsList) {
            logger.debug("DoiStaging: {}", ds);

            // TODO: dry-run first
            
            if (ds.isCompletelyValid()) {
                changed |= publish(projectConfig, ds);
            } else {
                final String doiSuffix = ds.getDoi().getIdentifier();
                logger.warn("Ignoring {} - DOI is not valid yet", doiSuffix);
            }
        }

        if (changed) {
            // Update Project:
            doiService.updateProjectDate(projectName, now);
        } else {
            throw new RuntimeException("Nothing to publish !");
        }
    }

    private boolean publish(final ProjectConfig projectConfig, final DoiStaging ds) throws IOException {
        boolean changed = false;

        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();

        /*
1.    reécrire les identifiants DOI (identifier et relatedIdentifier) pour remplacer le préfixe de test en préfixe OSUG à l'aide de la base de données
2.    copier la landing page (URL publiée chez datacite): PUBLIC
3.    gérer les redirections ie une URL alternative pour la landing page
4.    publier ce DOI avec le préfixe OSUG et mettre à jour la BDD
         */
        final String doiSuffix = ds.getDoi().getIdentifier();
        logger.info("processing {}", doiSuffix);

        final File stagingDoiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, true, doiSuffix); // staging

        if (!stagingDoiFile.exists()) {
            logger.warn("Ignoring {} - missing xml at {}", doiSuffix, stagingDoiFile);
        } else {
            changed = true;

            // Read meta data:
            final String metadata = FileUtils.readFile(stagingDoiFile);

            final String publicMetadata = replacePrefix(metadata, doiConfig.getPrefix());

            final File fileDoi = FilePathUtils.getDoiMetaDataFile(projectConfig, false, doiSuffix); // public
            FileUtils.writeFile(publicMetadata, fileDoi);

            // compute MD5 on metadata:
            final String xmlMd5 = FileUtils.MD5(new FileInputStream(fileDoi));
            logger.debug("DOI metadata [{}] MD5: {}", fileDoi, xmlMd5);

            doiService.createOrUpdateDoiPublic(ds, xmlMd5, now);
        }
        return changed;
    }

    private static String replacePrefix(final String metadata, final String prefix) {
        return metadata.replaceAll(Const.DOI_PREFIX_TEST, prefix);
    }
}
