/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.service.DataciteClient;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
@ConfigurationProperties(prefix = "datacite")
public class DataciteConfig {
    
    private final static Logger logger = LoggerFactory.getLogger(DataciteConfig.class.getName());

    /** User account (registered in datacite) */
    private String user;

    /** User password (registered in datacite) */
    private String password;
    
    /** enable / disable publication on datacite (official prefix) */
    private boolean enablePublic = false;
    
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;
    
    private DataciteClient dcClient;
    
    @PostConstruct
    public void initialize() {
        logger.info("Datacite user: [{}]", user);
        logger.info("Datacite enablePublic: [{}]", enablePublic);
        if (user != null) {
            restTemplateBuilder = restTemplateBuilder.basicAuthorization(user, password);
        }
        dcClient = new DataciteClient(restTemplateBuilder, enablePublic);
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public DataciteClient getClient() {
        return dcClient;
    }

    public boolean isEnablePublic() {
        return enablePublic;
    }

    public void setEnablePublic(final boolean enablePublic) {
        this.enablePublic = enablePublic;
    }
    
}
