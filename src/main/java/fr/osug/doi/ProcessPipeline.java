/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.NameEntry;
import fr.osug.doi.domain.Project;
import fr.osug.doi.domain.Status;
import fr.osug.doi.service.DoiService;
import fr.osug.doi.validation.ValidationUtil;
import fr.osug.util.FileUtils;
import fr.osug.xml.validator.ErrorMessage;
import fr.osug.xml.validator.ValidationResult;
import fr.osug.xml.validator.XmlValidator;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class ProcessPipeline extends AbstractPipeline<ProcessPipelineData> {

    private final static Logger logger = LoggerFactory.getLogger(ProcessPipeline.class.getName());

    public final static String XSD_DOI_SCHEMA = "file://" + Paths.DIR_XSD_SCHEMA + "/metadata.xsd";

    private final static String XSLT_XML_TO_DOI = "xml2doi.xsl";

    private final static String FILE_NAMES_CSV = "names" + Const.FILE_EXT_CSV;

    // members:
    private boolean saveCSV = false;

    public ProcessPipeline(final boolean saveCSV,
                           final ProcessPipelineData pipeData,
                           final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
        this.saveCSV = saveCSV;
    }

    @Override
    public void doExecute() throws IOException {
        for (ProjectConfig projectConfig : pipeData.getProjectConfigs()) {
            doExecute(projectConfig);
        }
    }

    protected void doExecute(final ProjectConfig projectConfig) throws IOException {
        logger.info("Process...");

        // preload all needed data:        
        projectConfig.prepareProcess();

        // reset process data:
        pipeData.reset();

        File[] files;

        // 1 - Process complete datasets (csv):
        files = CsvUtil.findCSVFiles(projectConfig.getMetadataDir());
        for (File inputCsvFile : files) {
            processCsvFile(projectConfig, inputCsvFile, false);
        }

        // 2 - Process partial datasets (csv to be merged with templates):
        files = CsvUtil.findCSVFiles(projectConfig.getInputDir());
        for (File inputCsvFile : files) {
            processCsvFile(projectConfig, inputCsvFile, true);
        }

        // TODO: 3 - process complete datasets as XML files (full metadata)
        // global validation of references
        validateRefs(projectConfig);

        // Update database:
        updateDatabase(projectConfig);
        logger.info("Process: done.");
    }

    private void updateDatabase(final ProjectConfig projectConfig) {
        logger.info("updateDatabase ...");

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;
        final Collection<ProcessPipelineDoiData> doiDataCollection = pipeData.getDoiDatas().values();

        final DoiService doiService = doiConfig.getDoiService();

        // Get or create the project:
        final Project project = doiService.getOrCreateProject(projectName, now);

        boolean changed = false;

        // Update Dois:
        for (ProcessPipelineDoiData doiData : doiDataCollection) {
            // Update DOI in staging phase:
            changed |= doiService.createOrUpdateDoiStaging(project,
                    doiData.getDoiSuffix(), doiData.getTitle(),
                    doiData.getMetadataMd5(), doiData.isValid(),
                    doiData.messagesToString(), now);
        }

        if (changed) {
            // Update Project:
            doiService.updateProjectDate(projectName, now);
        }

        logger.info("updateDatabase: done");
    }

    private void validateRefs(final ProjectConfig projectConfig) throws IOException {
        logger.debug("validateRefs");

        final DoiService doiService = doiConfig.getDoiService();
        final String publicPrefix = doiConfig.getPrefix();

        for (String doiId : pipeData.getDoiIds()) {
            logger.debug("checking refs for [{}]", doiId);

            final Set<String> refs = pipeData.getDoiRefs(doiId);
            if (refs != null) {
                for (String refId : refs) {
                    final boolean isPublic = refId.startsWith(publicPrefix);
                    // Only check DOI references with Prefix Test or Public:
                    if (ValidationUtil.isTestPrefix(refId) || isPublic) {
                        logger.debug("validateRefs: check ref[{}]", refId);

                        boolean found = false;

                        // Lookup first in processed identifiers:
                        if (pipeData.hasDoiId(refId)) {
                            found = true;
                        } else {
                            // Then in database (previous data):
                            final String doiSuffix = DoiCsvData.getDoiSuffix(refId);

                            final Doi doi = doiService.getDoi(doiSuffix);
                            if (doi != null) {
                                if (isPublic && doi.getStatus() != Status.PUBLIC) {
                                    logger.warn("validateRefs: DOI[{}] is not public yet (use test prefix instead)", refId);
                                    found = false;
                                } else {
                                    found = true;
                                }
                            }
                        }
                        if (!found) {
                            final ProcessPipelineDoiData doiData = pipeData.getDoiData(doiId);
                            if (doiData != null) {
                                doiData.addError("Invalid " + Const.KEY_REL_ID_START + " '" + refId + "' found.");
                            }
                        }
                    }
                }
            }
        }

        // Check name references:
        final TreeSet<NameEntry> entries = pipeData.getNameRefs();
        if (logger.isDebugEnabled()) {
            logger.debug("name references:\n{}", entries);
        }

        // Collect unique attributes:
        final Set<String> keys = new LinkedHashSet<String>();
        for (NameEntry e : entries) {
            if (e.hasAttributes()) {
                for (String k : e.getAttributes().keySet()) {
                    keys.add(k);
                }
            }
        }

        final int nCols = keys.size() + 1;

        final List<String[]> rows = new ArrayList<String[]>();
        // header:
        String[] cols = new String[nCols];
        int i = 0;
        cols[i++] = "#Name";
        for (String k : keys) {
            cols[i++] = k;
        }
        rows.add(cols);
        // data:
        for (NameEntry e : entries) {
            if (e.hasAttributes()) {
                cols = new String[nCols];
                i = 0;
                cols[i++] = e.getName();
                for (String k : keys) {
                    cols[i++] = e.getAttribute(k);
                }
            } else {
                cols = new String[]{e.getName()};
            }
            rows.add(cols);
        }
        final CsvData data = new CsvData(rows);
        if (logger.isDebugEnabled()) {
            logger.debug("name reference table:\n{}", data);
        }

        // Save file as CSV:
        final File fileCSV = new File(projectConfig.getTmpDir(), FILE_NAMES_CSV);
        CsvUtil.write(data, fileCSV);
    }

    private void processCsvFile(final ProjectConfig projectConfig, final File inputCsvFile,
                                final boolean doMerge) throws IOException {
        /*
        Spliter et produire 1 fichier XML dataCite par jeu.
        Ex: AMMA-CATCH.CL.Run_O.xml
         */
        final List<DoiCsvData> csvDatas = DoiCsvSplitter.split(inputCsvFile);

        for (DoiCsvData data : csvDatas) {
            if (logger.isDebugEnabled()) {
                logger.debug("process: data:\n{}", data);
            }
            if (!data.isEmpty()) {
                final String doiId = data.getDoiId();

                // check identifier (missing) ?
                if (ValidationUtil.checkIdentifier(doiId, data)) {
                    final String doiSuffix = data.getDoiSuffix();
                    logger.info("process: dataset[{}].", doiSuffix);

                    final ProcessPipelineDoiData doiData = pipeData.newDoiData(doiId, doiSuffix, data.getTitle());

                    // ignore duplicates
                    if (doiData != null) {
                        // validate identifier (format)
                        ValidationUtil.validateIdentifier(doiId, doiData);

                        if (doMerge) {
                            mergeCSV(projectConfig, data, doiData);
                        }

                        // Upgrade metadata:
                        upgradeData(data, doiData.getDoiId());

                        // Validate metadata:
                        validateData(projectConfig, data, doiData);

                        // Save metadata for dataset:
                        saveData(projectConfig, data, doiData);
                    }
                }
            }
        }
    }

    private void mergeCSV(final ProjectConfig projectConfig, final DoiCsvData data, final ProcessPipelineDoiData doiData) throws IOException {

        final DoiTemplates templates = projectConfig.getTemplates();

        /*                
        ### 2/ Fusionner avec template observatoire : template_obs.csv

        Informations disjointes
         */
        DoiCsvData t = templates.getBase();
        if (t == null) {
            doiData.addError("Missing base template for project '" + projectConfig.getProjectName() + "'");
        } else {
            data.mergeFrom(t);
        }

        final String doiId = data.getDoiId(); // not null

        if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_USE_TEMPLATE_COUNTRY)) {
            /*
            ### 3/ Traitement spécifiques: fusionner avec template pays

            Fusionner avec template pays (template_benin.csv):

            Trouver le template du pays qui contient geoLocationPlace;Benin
             */
            final String geoLocationPlace = data.getFirstGeoLocationPlace();
            if (geoLocationPlace == null) {
                doiData.addWarning("Missing ''" + Const.KEY_GEO_LOCATION_PLACE + "'' in dataset '" + doiId + "'");
            } else {
                t = templates.getByGeoLocationPlace(geoLocationPlace);
                if (t == null) {
                    doiData.addError("Missing country template for '" + geoLocationPlace + "'");
                } else {
                    logger.info("Using country template '{}'", geoLocationPlace);
                    data.mergeFrom(t);
                }
            }
        }

        if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_USE_TEMPLATE_DOI)) {
            /*
            ### 4/ Traitement spécifiques: fusionner avec template jeu

            Trouver le nom du jeu dans le XML:
            <identifier identifierType="DOI">10.5072/AMMA-CATCH.CL.Run_O</identifier>

            Trouver le template du jeu qui contient identifier:DOI;10.5072/AMMA-CATCH.CL.Run_O
             */
            t = templates.getByIdentifier(doiId);
            if (t == null) {
                doiData.addWarning("Missing dataset template for id '" + doiId + "'");
            } else {
                logger.info("Using dataset template for id '{}'", doiId);
                data.mergeFrom(t);
            }
        }
    }

    private void saveData(final ProjectConfig projectConfig, final DoiCsvData data, final ProcessPipelineDoiData doiData) throws IOException {
        // Sort keys:
        data.sort();

        // Get temporary directory:
        final File tmpDoiDir = projectConfig.getTmpDoiDir();

        final String doiSuffix = data.getDoiSuffix();

        // Save file as XML:
        DoiCsvToXml.write(data, pipeData.buffer);

        final String xmlDoc = pipeData.buffer.toString();
        logger.debug("xmlDoc: \n{}", xmlDoc);

        boolean forceSaveCSV = false;
        boolean forceSaveXML = false;

        // check non empty file:
        if (xmlDoc.length() == 0) {
            forceSaveCSV = true;
            doiData.addError("No XML output");
        } else {
            pipeData.doiDoc.reset();

            // use XSLT to write doi xml file:
            doiConfig.getXmlFactory().transform(
                    new StreamSource(new StringReader(xmlDoc)),
                    Paths.DIR_XSL + XSLT_XML_TO_DOI, true,
                    new StreamResult(pipeData.doiDoc)); // no xslt-param

            final String doiDoc = pipeData.doiDoc.toString();
            logger.debug("doiDoc: \n{}", doiDoc);

            if (doiDoc.length() == 0) {
                forceSaveXML = true;
                doiData.addError("No Metadata XML output");
            } else {
                final File fileDoi = new File(tmpDoiDir, doiSuffix + Const.FILE_EXT_XML);
                FileUtils.writeFile(doiDoc, fileDoi);

                // compute MD5 on metadata:
                final String xmlMd5 = FileUtils.MD5(new FileInputStream(fileDoi));
                doiData.setMetadataMd5(xmlMd5);
                logger.debug("DOI metadata [{}] MD5: {}", fileDoi, xmlMd5);

                // Validate:
                final XmlValidator validator = doiConfig.getXmlValidatorFactory().getInstance(XSD_DOI_SCHEMA);

                final ValidationResult vr = new ValidationResult();
                validator.validate(new StreamSource(fileDoi), vr);

                logger.info("XSD validation [{}]: {}", fileDoi.getAbsolutePath(), vr.isValid());

                if (!vr.isValid()) {
                    doiData.addError("Metadata XML validation failed:");

                    for (ErrorMessage msg : vr.getMessages()) {
                        doiData.addError(msg.toString());
                    }
                }

                // Anyway copy the doi metadata into the data/staging directory:
                final File stagingDoiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, true, doiSuffix);
                logger.debug("Copy [{}] to [{}]", fileDoi, stagingDoiFile);

                FileUtils.copy(fileDoi, stagingDoiFile);
            }
        }

        // Always save intermediate results in case of failure:
        if (forceSaveCSV || this.saveCSV) {
            // Save file as CSV:
            final File fileCSV = new File(tmpDoiDir, doiSuffix + Const.FILE_EXT_CSV);
            CsvUtil.write(data, fileCSV);
        }

        if (forceSaveXML || doiConfig.isDebug()) {
            final File fileXML = new File(tmpDoiDir, doiSuffix + "-xml" + Const.FILE_EXT_XML);
            FileUtils.writeFile(xmlDoc, fileXML);
        }
    }

    private void validateData(final ProjectConfig projectConfig, final DoiCsvData data, final ProcessPipelineDoiData doiData) throws IOException {
        logger.debug("validateData [{}]", doiData.getDoiId());

        // check title
        if (data.getTitle().isEmpty()) {
            doiData.addWarning("Empty title.");
        }

        // check all values:
        for (ListIterator<String[]> it = data.getRows().listIterator(); it.hasNext();) {
            String[] cols = it.next();
            if (cols != null && cols.length >= 2) {
                if (cols[1].toLowerCase().contains("todo")) {
                    doiData.addWarning("TODO detected for key [" + cols[0] + ']');
                }

                // Collect name references and override specific name entries:
                final String key = cols[0];
                if (key.startsWith(Const.KEY_CREATOR_NAME)
                        || key.startsWith(Const.KEY_CONTRIBUTOR_NAME)
                        || key.startsWith(Const.KEY_FUNDER_NAME)) {

                    final String name = cols[1];
                    final String simpleName = ValidationUtil.simplifyName(name);

                    // Check in override names:
                    final NameEntry override = projectConfig.getOverrideNameEntry(simpleName);

                    if (override != null) {
                        logger.debug("override: {}", override);
                        cols = NameEntry.toCsv(override);
                        cols[0] = key;

                        it.set(cols);
                    }

                    pipeData.addNameRef(cols);
                }
            }
        }

        final Set<String> refs = data.getReferences();
        if (refs != null) {
            logger.debug("References: {}", refs);
        }
        // anyway: add the doi entry:
        pipeData.addRefs(doiData.getDoiId(), refs);
    }

    public static void upgradeData(final DoiCsvData data, final String id) throws IOException {
        if (Const.F_SCHEMA_VERSION >= Const.F_SCHEMA_VERSION_4_1) {
            logger.debug("upgradeData[{}] to schema {}", id, Const.SCHEMA_VERSION);

            for (ListIterator<String[]> it = data.getRows().listIterator(); it.hasNext();) {
                String[] cols = it.next();
                if (cols != null && cols.length >= 2) {
                    final String key = cols[0];
                    
                    // Convert 'contributorName:Funder' (deprecated ContributorType=Funder) to 'funderName' (fundingReference)
                    if (Const.KEY_CONTRIBUTOR_FUNDER_NAME.equals(key)) {
                        if (cols.length > 2) {
                            logger.warn("upgradeData[{}] : Complex Contributor::Funder = {}", Arrays.toString(cols));
                            // trim (optional attributes like affiliation or orcid)
                            final String value = cols[1];
                            cols = new String[2];
                            cols[1] = value;
                        }
                        cols[0] = Const.KEY_FUNDER_NAME;
                        
                        logger.info("upgradeData[{}]: Fixed Funder: '{}'", id, cols[1]);
                        it.set(cols);
                    }
                }
            }
        }
    }
}
