/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.NameEntry;
import fr.osug.doi.validation.ValidationUtil;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public final class ProjectConfig {

    private final static Logger logger = LoggerFactory.getLogger(ProjectConfig.class.getName());

    public final static String CONF_PROJECT = "project.properties";

    public final static String CONFIG_ACCESS_INSTRUCTIONS = "access_instruction.html";
    public final static String CONFIG_ACCESS_INSTRUCTIONS_ALT = "access_instruction_alt.html";
    public final static String CONFIG_ACCESS_INSTRUCTIONS_DOI = "access_instruction_$DOI.html";
    public final static String KEY_CONFIG_ACCESS_INSTRUCTIONS_DOI = "$DOI";
    
    public final static String CONF_URL_MAP_DATA_ACCESS = "doi_url_data_access.csv";
    public final static String CONF_URL_MAP_DATA_ACCESS_ALT = "doi_url_data_access_alt.csv";
    public final static String CONF_URL_MAP_LANDING_PAGE = "doi_url_landing_page.csv";

    private final static String CONF_OVERRIDE_NAMES_CSV = "names_override" + Const.FILE_EXT_CSV;

    /* project.properties */
    /** use templates for countries = "use_template_country" */
    public final static String CONF_KEY_USE_TEMPLATE_COUNTRY = "use_template_country";
    /** use templates for each DOI = "use_template_doi" */
    public final static String CONF_KEY_USE_TEMPLATE_DOI = "use_template_doi";

    /** general data access url = "dataAccessUrl" */
    public final static String CONF_KEY_DATA_ACCESS_URL = "dataAccessUrl";
    /** general data access alternative url = "dataAccessAltUrl" */
    public final static String CONF_KEY_DATA_ACCESS_ALT_URL = "dataAccessAltUrl";
    /** generate embedded fragments = "generate_embedded" */
    public final static String CONF_KEY_GENERATE_EMBEDDED = "generate_embedded";

    /* members */
    private final String projectName;
    private final File projectConf;

    // lazy:
    private Properties properties = null;
    private Map<String, String> urlMapDataAccess = null;
    private Map<String, String> urlMapDataAccessAlt = null;
    private Map<String, String> urlMapLandingPages = null;
    // Directories:
    private File metadataDir = null;
    private File inputDir = null;
    private File tmpDir;
    private File tmpDoiDir;
    private File stagingDir;
    private File publicDir;
    private File webStagingProjectDir;
    private File webStagingProjectEmbedDir;
    private File webStagingProjectXmlDir;
    private File webPublicProjectDir;
    private File webPublicProjectEmbedDir;
    private File webPublicProjectXmlDir;
    // Process state:
    private DoiTemplates templates = null;
    private Map<String, NameEntry> overrideNameEntryMap = null;

    public ProjectConfig(final PathConfig pathConfig, final String projectName) throws IOException {
        this(pathConfig, projectName, true);
    }

    public ProjectConfig(final PathConfig pathConfig, final String projectName, final boolean required) throws IOException {
        this.projectName = projectName;

        // may fail:
        this.projectConf = dir(absPath(Paths.DIR_CONFIG, projectName), required);
        logger.info("Project Config: {}", projectConf);

        if (required) {
            // preload properties to ensure the folder contains the property file:
            getProperties();
        }

        initInputDir(absPath(this.projectConf, Paths.DIR_PROJECT_INPUTS), required);
        initMetadataDir(absPath(this.projectConf, Paths.DIR_PROJECT_METADATA), required);

        initTmpDir(absPath(Paths.DIR_TMP, projectName));
        initTmpDoiDir(absPath(tmpDir, Paths.DIR_TMP_DOI));

        initStagingDir(absPath(Paths.DIR_STAGING, projectName));
        initPublicDir(absPath(Paths.DIR_PUBLIC, projectName));

        // /www/staging directories:
        initWebStagingProjectDir(absPath(pathConfig.getWebStagingDir(), projectName));
        initWebStagingProjectEmbedDir(absPath(webStagingProjectDir, Paths.DIR_WEB_EMBED));
        initWebStagingProjectXmlDir(absPath(webStagingProjectDir, Paths.DIR_WEB_XML));

        // /www/public directories:
        initWebPublicProjectDir(absPath(pathConfig.getWebPublicDir(), projectName));
        initWebPublicProjectEmbedDir(absPath(webPublicProjectDir, Paths.DIR_WEB_EMBED));
        initWebPublicProjectXmlDir(absPath(webPublicProjectDir, Paths.DIR_WEB_XML));
    }

    public String dumpProjectDirs() {
        final StringBuilder sb = new StringBuilder(1024);

        sb.append(getProjectConf()).append('\n');
        sb.append(getInputDir()).append('\n');
        sb.append(getMetadataDir()).append('\n');
        sb.append(getTemplatesDir()).append('\n');

        sb.append(getTmpDir()).append('\n');
        sb.append(getTmpDoiDir()).append('\n');

        sb.append(getStagingDir()).append('\n');
        sb.append(getPublicDir()).append('\n');

        // /www/staging directories:
        sb.append(getWebStagingProjectDir()).append('\n');
        sb.append(getWebPublicProjectEmbedDir()).append('\n');
        sb.append(getWebPublicProjectXmlDir()).append('\n');

        // /www/public directories:
        sb.append(getWebPublicProjectDir()).append('\n');
        sb.append(getWebPublicProjectEmbedDir()).append('\n');
        sb.append(getWebPublicProjectXmlDir()).append('\n');

        return sb.toString();
    }

    public void prepareProcess() throws IOException {
        // preload needed data for the process pipeline:
        getTemplates();
        getUrlMapDataAccess();
        getUrlMapLandingPage();
        getOverrideNameMap();
    }

    public void initInputDir(final String dirPath, final boolean required) throws IOException {
        this.inputDir = dir(dirPath, required);
    }

    public void initMetadataDir(final String dirPath, final boolean required) throws IOException {
        this.metadataDir = dir(dirPath, required);
    }

    public void initTmpDir(final String dirPath) throws IOException {
        this.tmpDir = FileUtils.createDirectories(dirPath);
    }

    public void initTmpDoiDir(final String dirPath) throws IOException {
        this.tmpDoiDir = FileUtils.createDirectories(dirPath);
    }

    public void initStagingDir(final String dirPath) throws IOException {
        this.stagingDir = FileUtils.createDirectories(dirPath);
    }

    public void initPublicDir(final String dirPath) throws IOException {
        this.publicDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebStagingProjectDir(final String dirPath) throws IOException {
        this.webStagingProjectDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebStagingProjectEmbedDir(final String dirPath) throws IOException {
        this.webStagingProjectEmbedDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebStagingProjectXmlDir(final String dirPath) throws IOException {
        this.webStagingProjectXmlDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebPublicProjectDir(final String dirPath) throws IOException {
        this.webPublicProjectDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebPublicProjectEmbedDir(final String dirPath) throws IOException {
        this.webPublicProjectEmbedDir = FileUtils.createDirectories(dirPath);
    }

    public void initWebPublicProjectXmlDir(final String dirPath) throws IOException {
        this.webPublicProjectXmlDir = FileUtils.createDirectories(dirPath);
    }

    public String getProjectName() {
        return projectName;
    }

    /* properties */
    private Properties getProperties() {
        if (properties == null) {
            properties = loadProperties(new File(projectConf, CONF_PROJECT));
        }
        return properties;
    }

    public String getProperty(final String key) {
        String val = getProperties().getProperty(key);
        if (val != null) {
            val = val.trim();
            if (val.length() != 0) {
                return val;
            }
        }
        return null;
    }

    public boolean getPropertyBoolean(final String key) {
        return getPropertyBoolean(key, false);
    }

    public boolean getPropertyBoolean(final String key, final boolean def) {
        final String val = getProperties().getProperty(key);
        if (val != null) {
            return Boolean.valueOf(val);
        }
        return def;
    }

    /* DataAccess Url Mapping */
    private Map<String, String> getUrlMapDataAccess() throws IOException {
        if (urlMapDataAccess == null) {
            urlMapDataAccess = CsvUtil.loadUrlMapping(new File(this.projectConf, CONF_URL_MAP_DATA_ACCESS));
        }
        return urlMapDataAccess;
    }

    public String getUrlDataAccess(final String doiSuffix) throws IOException {
        return getUrlMapDataAccess().get(doiSuffix);
    }

    /* DataAccess Alternative Url Mapping */
    private Map<String, String> getUrlMapDataAccessAlt() throws IOException {
        if (urlMapDataAccessAlt == null) {
            urlMapDataAccessAlt = CsvUtil.loadUrlMapping(new File(this.projectConf, CONF_URL_MAP_DATA_ACCESS_ALT));
        }
        return urlMapDataAccessAlt;
    }

    public String getUrlDataAccessAlt(final String doiSuffix) throws IOException {
        return getUrlMapDataAccessAlt().get(doiSuffix);
    }

    /* LandingPage Url Mapping */
    private Map<String, String> getUrlMapLandingPage() throws IOException {
        if (urlMapLandingPages == null) {
            urlMapLandingPages = CsvUtil.loadUrlMapping(new File(this.projectConf, CONF_URL_MAP_LANDING_PAGE));
        }
        return urlMapLandingPages;
    }

    public String getUrlLandingPage(final String doiSuffix) throws IOException {
        return getUrlMapLandingPage().get(doiSuffix);
    }

    /* templates */
    public DoiTemplates getTemplates() throws IOException {
        if (templates == null) {
            templates = new DoiTemplates(getTemplatesDir());
        }
        return templates;
    }

    /* names override */
    private Map<String, NameEntry> getOverrideNameMap() throws IOException {
        if (overrideNameEntryMap == null) {
            overrideNameEntryMap = loadOverrideNames(new File(this.projectConf, CONF_OVERRIDE_NAMES_CSV));
        }
        return overrideNameEntryMap;
    }

    public NameEntry getOverrideNameEntry(final String simpleName) throws IOException {
        return getOverrideNameMap().get(simpleName);
    }

    /* Directories */
    public File getProjectConf() {
        return projectConf;
    }

    private String getTemplatesDir() {
        return absPath(this.projectConf, Paths.DIR_PROJECT_TEMPLATES);
    }

    public File getMetadataDir() {
        return metadataDir;
    }

    public File getInputDir() {
        return inputDir;
    }

    public File getTmpDir() {
        return tmpDir;
    }

    public File getTmpDoiDir() {
        return tmpDoiDir;
    }

    public File getStagingDir() {
        return stagingDir;
    }

    public File getPublicDir() {
        return publicDir;
    }

    public File getWebStagingProjectDir() {
        return webStagingProjectDir;
    }

    public File getWebStagingProjectEmbedDir() {
        return webStagingProjectEmbedDir;
    }

    public File getWebStagingProjectXmlDir() {
        return webStagingProjectXmlDir;
    }

    public File getWebPublicProjectDir() {
        return webPublicProjectDir;
    }

    public File getWebPublicProjectEmbedDir() {
        return webPublicProjectEmbedDir;
    }

    public File getWebPublicProjectXmlDir() {
        return webPublicProjectXmlDir;
    }

    private static Properties loadProperties(final File propFile) {
        logger.info("Loading {}", propFile);

        final Properties props = new Properties();
        Reader r = null;
        try {
            r = FileUtils.reader(propFile);
            props.load(r);
        } catch (IOException ioe) {
            throw new RuntimeException("loadProperties failed:" + propFile.getAbsolutePath(), ioe);
        } finally {
            FileUtils.closeFile(r);
        }
        logger.info("properties: {}", props);
        return props;
    }

    private static Map<String, NameEntry> loadOverrideNames(final File csvFile) throws IOException {
        Map<String, NameEntry> nameEntryMap = Collections.emptyMap();

        if (csvFile.exists()) {
            final CsvData data = CsvUtil.read(csvFile);
            logger.debug("CSV data: {}", data);

            final List<String[]> rows = data.getRows();
            final int nbRows = rows.size();

            if (nbRows > 1) {
                // Parse header:
                String[] keys = null;
                String[] cols = rows.get(0);

                if (cols == null || cols.length < 1) {
                    logger.error("Missing header in {}", csvFile);
                } else {
                    keys = cols;
                    // Check keys:
                    for (int i = 0; i < keys.length; i++) {
                        String key = keys[i];

                        if (key.length() > 1) {
                            if (key.charAt(0) == Const.COMMENT) {
                                key = key.substring(1);
                            }
                            key = key.trim();
                        }
                        if (key.isEmpty()) {
                            logger.error("Missing column name at {}", i);
                            key = null; // ignore
                        }
                        keys[i] = key;
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("keys: {}", Arrays.toString(keys));
                    }

                    nameEntryMap = new LinkedHashMap<String, NameEntry>(nbRows - 1);

                    // Get mappings [Name|...]
                    for (int i = 1; i < nbRows; i++) {
                        cols = rows.get(i);

                        if (cols == null || cols.length < 1) {
                            continue;
                        }
                        String name = cols[0].trim();
                        if (name.isEmpty() || name.charAt(0) == Const.COMMENT) {
                            continue;
                        }

                        Map<String, String> attributes = null;

                        if (cols.length > 1) {
                            attributes = new HashMap<String, String>(4);

                            for (int j = 1; j < cols.length; j++) {
                                String key = keys[j];
                                String val = cols[j];
                                if (key != null && !val.trim().isEmpty()) {
                                    attributes.put(key, val);
                                }
                            }
                        }

                        final String simpleName = ValidationUtil.simplifyName(name);

                        nameEntryMap.put(simpleName, new NameEntry(name, attributes));
                    }
                }
            }
        }
        logger.debug("nameEntryMap: {}", nameEntryMap);
        return nameEntryMap;
    }

    private File dir(final String dirPath, final boolean required) throws IOException {
        if (required) {
            return FileUtils.getRequiredDirectory(dirPath).getCanonicalFile();
        }
        return FileUtils.createDirectories(dirPath);
    }

    private static String absPath(final String dir, final String name) {
        return new File(dir, name).getAbsolutePath();
    }

    private static String absPath(final File dir, final String name) {
        return new File(dir, name).getAbsolutePath();
    }
}
