/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.doi.domain.Doi;
import fr.osug.doi.domain.DoiStaging;
import fr.osug.doi.domain.Project;
import fr.osug.doi.domain.IndexEntry;
import fr.osug.doi.repository.DoiStagingRepository;
import fr.osug.doi.repository.ProjectRepository;
import fr.osug.doi.service.DoiService;
import fr.osug.util.FileUtils;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import fr.osug.doi.domain.DoiCommon;
import fr.osug.doi.repository.DoiBaseRepository;
import java.util.Arrays;
import java.util.Date;

/**
 *
 */
public final class GeneratePipeline extends AbstractPipeline<PipelineCommonData> {

    private final static Logger logger = LoggerFactory.getLogger(GeneratePipeline.class.getName());

    public final static String HTML_EXT = ".html";
    public final static String HTML_INDEX = "index" + HTML_EXT;
    public final static String HTML_REPORT = "report" + HTML_EXT;
    public final static String HTML_ERROR = "error" + HTML_EXT;

    private final static String XSLT_DOI_TO_LANDING_PAGE = "doi2landing.xsl";

    private final static String XSLT_PROJECT_INDEX = "project_index.xsl";
    private final static String XSLT_PROJECT_REPORT = "project_report.xsl";

    // xslt parameters (common):
    private final static String XSLT_PARAM_DATE = "date";
    private final static String XSLT_PARAM_WEB_ROOT = "www_root";

    // xslt parameters (doi2landing):
    private final static String XSLT_PARAM_DATA_ACCESS_HTML = "data_access_html";
    private final static String XSLT_PARAM_DATA_ACCESS_ALT_HTML = "data_access_alt_html";
    private final static String XSLT_PARAM_DATA_ACCESS_DOI_HTML = "data_access_doi_html";
    private final static String XSLT_PARAM_DATA_ACCESS_URL = "data_access_url";
    private final static String XSLT_PARAM_DATA_ACCESS_ALT_URL = "data_access_alt_url";
    private final static String XSLT_PARAM_EMBEDDED = "embedded";
    private final static String XSLT_PARAM_EMBEDDED_FULL = "full";
    private final static String XSLT_PARAM_EMBEDDED_HEADER = "header";
    private final static String XSLT_PARAM_EMBEDDED_META = "meta";
    private final static String XSLT_PARAM_LANDING_PAGE_URL = "landing_page_url";
    private final static String XSLT_PARAM_METADATA_FILE = "metadataFile";

    // xslt parameters (index_project):
    private final static String XSLT_PARAM_PARENT = "parent";
    private final static String XSLT_PARAM_CURRENT = "current";

    // members
    private final boolean doStaging;
    private final boolean doPublic;

    public GeneratePipeline(final boolean doStaging,
                            final boolean doPublic,
                            final PipelineCommonData pipeData,
                            final AbstractPipeline<?> pipe) {
        super(pipeData, pipe);
        this.doStaging = doStaging;
        this.doPublic = doPublic;
    }

    public void execute(final String project) throws IOException {
        logger.info("doStaging: {}", doStaging);
        logger.info("doPublic:  {}", doPublic);

        final ProjectRepository pr = doiConfig.getDoiService().getProjectRepository();

        // all or project by project:
        final List<String> projectNames;
        if (project == null) {
            projectNames = pr.findAllNamesOrderByNameAsc();
        } else {
            projectNames = Arrays.asList(new String[]{project});
        }

        logger.info("projectNames: {}", projectNames);

        // Retrieve project configs:
        for (String projectName : projectNames) {
            // get existing project:
            final Project p = pr.findOneByName(projectName);

            if (p == null) {
                logger.warn("Unknown Project [{}]", projectName);
            } else {
                pipeData.getProjectConfigAll(p.getName());
            }
        }

        super.execute();
    }

    @Override
    public void doExecute() throws IOException {
        // 1 - Landing pages for selected project(s):
        for (ProjectConfig projectConfig : pipeData.getProjectConfigsAll()) {
            generateLandingPages(projectConfig);
        }
        // 2 - Update index pages:
        generateIndexPages();
    }

    private void generateLandingPages(final ProjectConfig projectConfig) throws IOException {
        logger.info("generateLandingPages ...");
        if (doStaging) {
            generateLandingPages(projectConfig, true);
        }
        if (doPublic) {
            generateLandingPages(projectConfig, false);
        }
        logger.info("generateLandingPages: done.");
    }

    private void generateLandingPages(final ProjectConfig projectConfig,
                                      final boolean isStaging) throws IOException {
        final File webDir;
        final File webEmbedDir;
        final File webXmlDir;
        if (isStaging) {
            webDir = projectConfig.getWebStagingProjectDir();
            webEmbedDir = projectConfig.getWebStagingProjectEmbedDir();
            webXmlDir = projectConfig.getWebStagingProjectXmlDir();
        } else {
            webDir = projectConfig.getWebPublicProjectDir();
            webEmbedDir = projectConfig.getWebPublicProjectEmbedDir();
            webXmlDir = projectConfig.getWebPublicProjectXmlDir();
        }
        logger.info("Generating landing pages into {}", webDir);

        // see ProcessUrlPipeline:
        final File dataAccessFile = FileUtils.getFile(
            new File(projectConfig.getProjectConf(), ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS)
        );
        final File dataAccessAltFile = FileUtils.getFile(
            new File(projectConfig.getProjectConf(), ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS_ALT)
        );

        final Map<String, Object> xslParameters = new HashMap<String, Object>(8);
        xslParameters.put(XSLT_PARAM_DATE, pipeData.now.toString());
        if (dataAccessFile != null) {
            xslParameters.put(XSLT_PARAM_DATA_ACCESS_HTML, dataAccessFile.toURI().toString());
        }
        if (dataAccessAltFile != null) {
            xslParameters.put(XSLT_PARAM_DATA_ACCESS_ALT_HTML, dataAccessAltFile.toURI().toString());
        }

        final String projectName = projectConfig.getProjectName();
        final Date now = pipeData.now;

        final DoiService doiService = doiConfig.getDoiService();
        final DoiBaseRepository<?> dbr = doiService.getDoiCommonRepository(isStaging);

        final List<? extends DoiCommon> dList = dbr.findByProject(projectName);

        logger.debug("DoiCommon list for project[{}]: {}", projectName, dList);

        boolean changed = false;

        for (DoiCommon d : dList) {
            logger.debug("DoiCommon: {}", d);

            final String doiSuffix = d.getDoi().getIdentifier();
            logger.info("processing {}", doiSuffix);
            
            // Get (optional) specific data access instructions to DOI:
            final File dataAccessDoiFileLocation = new File(projectConfig.getProjectConf(), 
                    ProjectConfig.CONFIG_ACCESS_INSTRUCTIONS_DOI.replace(ProjectConfig.KEY_CONFIG_ACCESS_INSTRUCTIONS_DOI, doiSuffix));
            
            final File dataAccessDoiFile = FileUtils.getFile(dataAccessDoiFileLocation);
            if (dataAccessDoiFile != null) {
                logger.debug("dataAccessDoiFileLocation: {}", dataAccessDoiFileLocation);
                xslParameters.put(XSLT_PARAM_DATA_ACCESS_DOI_HTML, dataAccessDoiFile.toURI().toString());
            } else {
                xslParameters.remove(XSLT_PARAM_DATA_ACCESS_DOI_HTML);
            }

            // Get specific data access URL:
            final String dataAccessUrl = d.getDataAccessUrl();
            if (dataAccessUrl != null) {
                logger.debug("dataAccessUrl: {}", dataAccessUrl);
                xslParameters.put(XSLT_PARAM_DATA_ACCESS_URL, dataAccessUrl);
            } else {
                xslParameters.remove(XSLT_PARAM_DATA_ACCESS_URL);
            }

            // Get (optional) specific alternative data access URL:
            final String dataAccessAltUrl = d.getDataAccessAltUrl();
            if (dataAccessAltUrl != null) {
                logger.debug("dataAccessAltUrl: {}", dataAccessAltUrl);
                xslParameters.put(XSLT_PARAM_DATA_ACCESS_ALT_URL, dataAccessAltUrl);
            } else {
                xslParameters.remove(XSLT_PARAM_DATA_ACCESS_ALT_URL);
            }

            // Get external Landing page URL:
            final String landingPageUrl = d.getLandingExtUrl();
            if (landingPageUrl != null) {
                logger.debug("landingPageUrl: {}", landingPageUrl);
                xslParameters.put(XSLT_PARAM_LANDING_PAGE_URL, landingPageUrl);
            } else {
                xslParameters.remove(XSLT_PARAM_LANDING_PAGE_URL);
            }

            final File doiFile = FilePathUtils.getDoiMetaDataFile(projectConfig, isStaging, doiSuffix);

            if (!doiFile.exists()) {
                if (isStaging) {
                    logger.info("Ignoring {} - no xml", doiSuffix);
                } else {
                    logger.warn("Ignoring {} - missing xml at {}", doiSuffix, doiFile);
                }
            } else {
                final String landingPageFilePath = generateLandingPages(projectConfig, doiFile, xslParameters, webDir, webEmbedDir, webXmlDir);

                final String locUrl = extractUrlPath(landingPageFilePath);

                // Update landing local url:
                if (isStaging) {
                    changed |= doiService.updateStagingDoiLocUrl(doiSuffix, locUrl, now);
                } else {
                    changed |= doiService.updatePublicDoiLocUrl(doiSuffix, locUrl, now);
                }
            }
        }

        if (changed) {
            // Update Project:
            doiService.updateProjectDate(projectName, now);
        }
    }

    private String generateLandingPages(final ProjectConfig projectConfig,
                                        final File xmlFileDOI,
                                        final Map<String, Object> xslParameters,
                                        final File webDir,
                                        final File webEmbedDir,
                                        final File webXmlDir) throws IOException {

        // Note: must be in synch with RemovePipeline.removeLandingPages()
        final String filenameNoExt = FileUtils.getFileNameWithoutExtension(xmlFileDOI);

        // main landing page:
        // [public/staging]/PROJECT/DOI_SUFFIX.html
        xslParameters.remove(XSLT_PARAM_EMBEDDED);
        xslParameters.put(XSLT_PARAM_WEB_ROOT, "../../");
        File outputFile = new File(webDir, filenameNoExt + HTML_EXT);
        generateLandingPage(xslParameters, xmlFileDOI, outputFile);

        // Store reference of the main landing page:
        final String landingPageFilePath = outputFile.getAbsolutePath();

        if (projectConfig.getPropertyBoolean(ProjectConfig.CONF_KEY_GENERATE_EMBEDDED)) {
            // embedded mode:
            xslParameters.put(XSLT_PARAM_WEB_ROOT, "../../../");

            // embedded mode - full:
            // [public/staging]/PROJECT/embed/DOI_SUFFIX[.html]
            outputFile = new File(webEmbedDir, filenameNoExt + HTML_EXT);
            xslParameters.put(XSLT_PARAM_EMBEDDED, XSLT_PARAM_EMBEDDED_FULL);
            generateLandingPage(xslParameters, xmlFileDOI, outputFile);

            // embedded mode - header:
            outputFile = new File(webEmbedDir, filenameNoExt + "-header" + HTML_EXT);
            xslParameters.put(XSLT_PARAM_EMBEDDED, XSLT_PARAM_EMBEDDED_HEADER);
            generateLandingPage(xslParameters, xmlFileDOI, outputFile);

            // embedded mode - meta:
            outputFile = new File(webEmbedDir, filenameNoExt + "-meta" + HTML_EXT);
            xslParameters.put(XSLT_PARAM_EMBEDDED, XSLT_PARAM_EMBEDDED_META);
            generateLandingPage(xslParameters, xmlFileDOI, outputFile);
        }

        // Xml DOI:
        outputFile = new File(webXmlDir, xmlFileDOI.getName());
        FileUtils.copy(xmlFileDOI, outputFile);

        return landingPageFilePath;
    }

    private void generateLandingPage(final Map<String, Object> xslParameters,
                                     final File inputFileDOI, final File outputFile) {

        xslParameters.put(XSLT_PARAM_METADATA_FILE, inputFileDOI.getName());

        // use XSLT to generate landing page from the doi xml file:
        transform(inputFileDOI, Paths.DIR_XSL + XSLT_DOI_TO_LANDING_PAGE, xslParameters, outputFile);
    }

    private void generateIndexPages() throws IOException {
        logger.info("generateIndexPages...");
        if (doStaging) {
            generateIndexPages(true);
        }
        if (doPublic) {
            generateIndexPages(false);
        }
        logger.info("generateIndexPages: done");
    }

    private void generateIndexPages(final boolean isStaging) {
        final String folderType;
        final File webDir;
        if (isStaging) {
            folderType = Paths.DIR_WEB_STAGING;
            webDir = doiConfig.getPathConfig().getWebStagingDir();
        } else {
            folderType = Paths.DIR_WEB_PUBLIC;
            webDir = doiConfig.getPathConfig().getWebPublicDir();
        }
        logger.info("Generating index pages into {}", webDir);

        final DoiService doiService = doiConfig.getDoiService();
        final ProjectRepository pr = doiService.getProjectRepository();
        final DoiBaseRepository<?> dbr = doiService.getDoiCommonRepository(isStaging);
        final DoiStagingRepository dsr = doiService.getDoiStagingRepository();

        final List<Project> projects = pr.findAllByOrderByNameAsc();

        // Project listing:
        List<IndexEntry> entries = new ArrayList<IndexEntry>();

        // index.html:
        for (Project p : projects) {
            final String projectName = p.getName();
            final Long countDoi = dbr.countByProject(projectName);

            if (countDoi > 0L) {
                final IndexEntry ie = new IndexEntry(projectName, p.getDescription(), p.getUpdateDate(), countDoi);

                logger.debug("IndexEntry: {}", ie);
                entries.add(ie);
            }
        }
        // serialize entries and transform in the /[staging|public]/index.html
        IndexUtil.write(entries, pipeData.buffer);

        String xmlDoc = pipeData.buffer.toString();
        logger.debug("xmlDoc(project index): \n{}", xmlDoc);

        final Map<String, Object> xslParameters = new HashMap<String, Object>(8);
        xslParameters.put(XSLT_PARAM_DATE, pipeData.now.toString());
        xslParameters.put(XSLT_PARAM_CURRENT, folderType);
        xslParameters.put(XSLT_PARAM_WEB_ROOT, "../");
        File outputFile = new File(webDir, HTML_INDEX);

        // use XSLT to generate the index page:
        transform(xmlDoc, Paths.DIR_XSL_ROOT + XSLT_PROJECT_INDEX, xslParameters, outputFile);

        // DOI listing per project:
        for (Project p : projects) {
            final String projectName = p.getName();

            // PROJECT/index.html:
            entries.clear();

            final List<? extends DoiCommon> dList = dbr.findByProject(projectName);

            logger.debug("DoiCommon list for project[{}]: {}", projectName, dList);

            if (!dList.isEmpty()) {
                for (DoiCommon d : dList) {
                    logger.debug("DoiBase: {}", d);
                    final Doi doi = d.getDoi();
                    final IndexEntry ie = new IndexEntry(doi.getIdentifier(), doi.getDescription(), d.getUpdateDate());

                    logger.debug("IndexEntry: {}", ie);
                    entries.add(ie);
                }
                // serialize entries and transform in the /[staging|public]/PROJECT/index.html
                IndexUtil.write(entries, pipeData.buffer);

                xmlDoc = pipeData.buffer.toString();
                logger.debug("xmlDoc(doi index): \n{}", xmlDoc);

                // inherit XSLT_PARAM_DATE:
                xslParameters.put(XSLT_PARAM_PARENT, folderType);
                xslParameters.put(XSLT_PARAM_CURRENT, projectName);
                xslParameters.put(XSLT_PARAM_WEB_ROOT, "../../");

                // suppose child directory is the project folder:
                final File webProjectDir = new File(webDir, projectName);
                outputFile = new File(webProjectDir, HTML_INDEX);

                // use XSLT to generate the PROJECT index page:
                transform(xmlDoc, Paths.DIR_XSL_ROOT + XSLT_PROJECT_INDEX, xslParameters, outputFile);

                if (isStaging) {
                    @SuppressWarnings("unchecked")
                    final List<DoiStaging> dsList = (List<DoiStaging>) dList;

                    // serialize doi staging list and transform in the /staging/PROJECT/report.html
                    IndexUtil.writeReport(dsList, pipeData.buffer);

                    xmlDoc = pipeData.buffer.toString();
                    logger.debug("xmlDoc(doi report): \n{}", xmlDoc);

                    // inherit XSLT_PARAM_DATE, XSLT_PARAM_PARENT, XSLT_PARAM_CURRENT, XSLT_PARAM_WEB_ROOT:
                    // suppose child directory is the project folder:
                    outputFile = new File(webProjectDir, HTML_REPORT);

                    // use XSLT to generate the PROJECT report page:
                    transform(xmlDoc, Paths.DIR_XSL_ROOT + XSLT_PROJECT_REPORT, xslParameters, outputFile);

                    // serialize doi staging error list and transform in the /staging/PROJECT/error.html
                    final List<DoiStaging> dsErrorList = dsr.findErrorByProject(projectName);
                    logger.debug("DoiStaging error list for project[{}]: {}", projectName, dsErrorList);

                    IndexUtil.writeReport(dsErrorList, pipeData.buffer);

                    xmlDoc = pipeData.buffer.toString();
                    logger.debug("xmlDoc(error report): \n{}", xmlDoc);

                    // inherit XSLT_PARAM_DATE, XSLT_PARAM_PARENT, XSLT_PARAM_CURRENT, XSLT_PARAM_WEB_ROOT:
                    // suppose child directory is the project folder:
                    outputFile = new File(webProjectDir, HTML_ERROR);

                    // use XSLT to generate the PROJECT error report page:
                    transform(xmlDoc, Paths.DIR_XSL_ROOT + XSLT_PROJECT_REPORT, xslParameters, outputFile);
                }
            }
        }
    }

    private void transform(final File src, final String xslFilePath,
                           final Map<String, Object> xslParameters, final File outputFile) {
        transform(new StreamSource(src), xslFilePath, xslParameters, outputFile);
    }

    private void transform(final String src, final String xslFilePath,
                           final Map<String, Object> xslParameters, final File outputFile) {
        transform(new StreamSource(new StringReader(src)), xslFilePath, xslParameters, outputFile);
    }

    private void transform(final StreamSource src, final String xslFilePath,
                           final Map<String, Object> xslParameters, final File outputFile) {

        logger.info("generate: {}", outputFile);

        doiConfig.getXmlFactory().transform(src, xslFilePath,
                xslParameters, true, new StreamResult(outputFile)
        );
    }

    private String extractUrlPath(final String landingPageFilePath) {
        final String webRootPath = doiConfig.getPathConfig().getWebRootPath();
        logger.debug("webRootPath: {}", webRootPath);

        if (!landingPageFilePath.startsWith(webRootPath)) {
            throw new IllegalStateException("Invalid web root path in file: " + landingPageFilePath);
        }
        return landingPageFilePath.substring(webRootPath.length() - 1);
    }
}
