
/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package org.datacite.schema.kernel_4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for box complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="box"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="westBoundLongitude" type="{http://datacite.org/schema/kernel-4}longitudeType"/&gt;
 *         &lt;element name="eastBoundLongitude" type="{http://datacite.org/schema/kernel-4}longitudeType"/&gt;
 *         &lt;element name="southBoundLatitude" type="{http://datacite.org/schema/kernel-4}latitudeType"/&gt;
 *         &lt;element name="northBoundLatitude" type="{http://datacite.org/schema/kernel-4}latitudeType"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "box", propOrder = {

})
public class Box {

    protected float westBoundLongitude;
    protected float eastBoundLongitude;
    protected float southBoundLatitude;
    protected float northBoundLatitude;

    /**
     * Gets the value of the westBoundLongitude property.
     * 
     */
    public float getWestBoundLongitude() {
        return westBoundLongitude;
    }

    /**
     * Sets the value of the westBoundLongitude property.
     * 
     */
    public void setWestBoundLongitude(float value) {
        this.westBoundLongitude = value;
    }

    /**
     * Gets the value of the eastBoundLongitude property.
     * 
     */
    public float getEastBoundLongitude() {
        return eastBoundLongitude;
    }

    /**
     * Sets the value of the eastBoundLongitude property.
     * 
     */
    public void setEastBoundLongitude(float value) {
        this.eastBoundLongitude = value;
    }

    /**
     * Gets the value of the southBoundLatitude property.
     * 
     */
    public float getSouthBoundLatitude() {
        return southBoundLatitude;
    }

    /**
     * Sets the value of the southBoundLatitude property.
     * 
     */
    public void setSouthBoundLatitude(float value) {
        this.southBoundLatitude = value;
    }

    /**
     * Gets the value of the northBoundLatitude property.
     * 
     */
    public float getNorthBoundLatitude() {
        return northBoundLatitude;
    }

    /**
     * Sets the value of the northBoundLatitude property.
     * 
     */
    public void setNorthBoundLatitude(float value) {
        this.northBoundLatitude = value;
    }

}
