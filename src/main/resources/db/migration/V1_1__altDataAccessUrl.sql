
/* DOI STAGING */
alter table doi_staging
    add column data_access_alt_url varchar(1024);

/* DOI PUBLIC */
alter table doi_public
    add column data_access_alt_url varchar(1024);

/* EOF */