/*******************************************************************************
 * OSUG-DOI project ( http://doi.osug.fr ) - Copyright (C) CNRS.
 ******************************************************************************/
package fr.osug.doi;

import fr.osug.xml.validator.ErrorMessage;
import fr.osug.xml.validator.ValidationResult;
import fr.osug.xml.validator.XmlValidator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.transform.stream.StreamSource;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = {"test", "debug"})
public class DOITextToXmlTest {

    private final static Logger logger = LoggerFactory.getLogger(DOITextToXmlTest.class.getName());

    /** absolute path to test folder to load test resources */
    private final static String DIR_TEST = Paths.DIR_BASE + "src/test/resources/";

    private final static String DIR_TEMPLATE = Paths.DIR_CONFIG + "/AMMA-CATCH/templates/";

    private final static String DIR_OUTPUT = Paths.DIR_BASE + "target/test-output/";
    private final static String DIR_STAGING = DIR_OUTPUT + "staging/";
    public final static String DIR_WEB = DIR_OUTPUT + "www/";

    @BeforeClass
    public static void setUpClass() {
    }

    // members:
    @Autowired
    private DOIConfig doiConfig;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSplitter() throws IOException {
        final List<DoiCsvData> csvDatas = DoiCsvSplitter.split(new File(DIR_TEST, "getRecords.csv"));

        for (DoiCsvData data : csvDatas) {
            logger.info("testSplitter:\n" + data);
        }
    }

    @Test
    public void testReadAndDump() throws IOException {
        dumpCSV(new File(DIR_TEMPLATE, "template_all_dataset.csv"));
    }

    @Test
    public void testReadTemplates() throws IOException {
        new DoiTemplates(DIR_TEMPLATE).getBase();
    }

    @Transactional
    @Test
    public void testPipeline() throws IOException {

        final String project = "AMMA-CATCH";

        final PathConfig pathConfig = doiConfig.getPathConfig();
        // redirect web:
        pathConfig.initWebRoot(DIR_WEB);

        final ProjectConfig projectConfig = new ProjectConfig(pathConfig, project);

        // redirect outputs:
        projectConfig.initInputDir(DIR_TEST, true);
        projectConfig.initMetadataDir(DIR_TEST + "xml/", true);
        projectConfig.initTmpDir(DIR_OUTPUT);

        projectConfig.initStagingDir(DIR_STAGING);

        final ProcessPipeline pipeline = doiConfig.getProcessPipeline(true, projectConfig);
        pipeline.execute();
    }

    @Test
    public void testXSLT() throws Exception {
        String res = doiConfig.getXmlFactory().transform(new StreamSource(new File(DIR_TEST + "AMMA-CATCH.all.xml")), Paths.DIR_XSL + "doi2landing.xsl", null, true);
        logger.info("test:\n" + res);
    }

    @Test
    public void testXSD() throws Exception {
        XmlValidator validator = doiConfig.getXmlValidatorFactory().getInstance(ProcessPipeline.XSD_DOI_SCHEMA);

        ValidationResult vr = validator.validate(new StreamSource(new File(DIR_TEST + "AMMA-CATCH.all.xml")));
        logger.info("validate: " + vr.isValid());
        if (!vr.isValid()) {
            for (ErrorMessage msg : vr.getMessages()) {
                logger.info(msg.toString());
            }
        }

        vr = validator.validate(new StreamSource(new File(Paths.DIR_XSL + "doi2csv.xsl")));
        logger.info("validate: " + vr.isValid());
        if (!vr.isValid()) {
            for (ErrorMessage msg : vr.getMessages()) {
                logger.info(msg.toString());
            }
        }
    }

    // Utilities
    private void dumpCSV(final File file) throws IOException {
        final CsvData data = CsvUtil.read(file);

        if (data.isEmpty()) {
            fail("The file [" + file + "] can not be read !");
        }

        logger.info("dumpCSV: [" + file + "] \n" + data);
    }

}
