<?xml version="1.0" encoding="utf-8"?>
<!--
   XSLT: this converts an XML file (rows/property) to a DOI (datacite XML) file
-->
<xsl:stylesheet version="1.0"
                xmlns="http://datacite.org/schema/kernel-4"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://datacite.org/schema/kernel-4"
                xmlns:gco="http://www.isotc211.org/2005/gco"
                xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gml="http://www.opengis.net/gml"
                xmlns:gmx="http://www.isotc211.org/2005/gmx"
                exclude-result-prefixes="d gmd gco gml gmx"
                xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4.1/metadata.xsd"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl">

    <xsl:output method="xml" indent="yes" omit-xml-declaration="no" media-type="text/xml" encoding="utf-8" standalone="yes"/>


    <!-- DOI metadata template -->
    <xsl:template match="/">
        <resource xsi:schemaLocation="http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4.1/metadata.xsd">
            <xsl:apply-templates select="./rows"/>
        </resource>
    </xsl:template>




    <xsl:template match="rows">
        <!--
        1	Identifier	1	identifier:DOI
        -->
        <xsl:apply-templates select="property[@key='identifier' and @attr1='DOI']" mode="identifier"/>

        <!--
        2.1	creatorName	1
        -->
        <xsl:if test="property[@key='creatorName']">
            <creators>
                <xsl:apply-templates select="property[@key='creatorName']" mode="creator"/>
            </creators>
        </xsl:if>

        <!--
        3	Title	1‐n	title
        -->
        <xsl:if test="property[@key='title']">
            <titles>
                <xsl:apply-templates select="property[@key='title']" mode="title"/>
            </titles>
        </xsl:if>

        <!--
        4	Publisher	1	publisher
        -->
        <xsl:apply-templates select="property[@key='publisher']" mode="publisher"/>

        <!--
        5	PublicationYear	1	publicationYear
        -->
        <xsl:apply-templates select="property[@key='publicationYear']" mode="publicationYear"/>

        <!--
        6	Subject	0-n	subject
        -->
        <xsl:if test="property[@key='subject']">
            <subjects>
                <xsl:apply-templates select="property[@key='subject']" mode="subject"/>
            </subjects>
        </xsl:if>

        <!--
        7	Contributor	0-n	"contributorName:ContactPerson..."
        -->
        <xsl:if test="property[@key='contributorName']">
            <contributors>
                <xsl:apply-templates select="property[@key='contributorName']" mode="contributor"/>
            </contributors>
        </xsl:if>

        <!--
        8	Date	0-n	"date:Collected|date:Created|date:Updated"
        -->
        <xsl:if test="property[@key='date']">
            <dates>
                <xsl:apply-templates select="property[@key='date']" mode="date"/>
            </dates>
        </xsl:if>

        <!--
        9	Language	0-1	language
        -->
        <xsl:apply-templates select="property[@key='language']" mode="language"/>

        <!--
        10	ResourceType	0-1	"resourceType:Dataset|resourceType:Service|resourceType:Software"
        -->
        <xsl:apply-templates select="property[@key='resourceType']" mode="resourceType"/>

        <!--
        11	AlternateIdentifier	0-n	-
        -->
        <xsl:if test="property[@key='alternateIdentifier']">
            <alternateIdentifiers>
                <xsl:apply-templates select="property[@key='alternateIdentifier']" mode="alternateIdentifier"/>
            </alternateIdentifiers>
        </xsl:if>

        <!--
        12	RelatedIdentifier	0-n	"relatedIdentifier:Cites:DOI..."
        -->
        <xsl:if test="property[@key='relatedIdentifier']">
            <relatedIdentifiers>
                <xsl:apply-templates select="property[@key='relatedIdentifier']" mode="relatedIdentifier"/>
            </relatedIdentifiers>
        </xsl:if>

        <!--
        13	Size	0-n	size
        -->
        <xsl:if test="property[@key='size']">
            <sizes>
                <xsl:apply-templates select="property[@key='size']" mode="size"/>
            </sizes>
        </xsl:if>

        <!--
        14	Format	0-n	format
        -->
        <xsl:if test="property[@key='format']">
            <formats>
                <xsl:apply-templates select="property[@key='format']" mode="format"/>
            </formats>
        </xsl:if>

        <!--
        15	Version	0-1	version
        -->
        <xsl:apply-templates select="property[@key='version']" mode="version"/>

        <!--
        16	Rights	0-n	rights
        -->
        <xsl:if test="property[@key='rights']">
            <rightsList>
                <xsl:apply-templates select="property[@key='rights']" mode="rights"/>
            </rightsList>
        </xsl:if>

        <!--
        17	Description	0-n	"description:Abstract..."
        -->
        <xsl:if test="property[@key='description']">
            <descriptions>
                <xsl:apply-templates select="property[@key='description']" mode="description"/>
            </descriptions>
        </xsl:if>

        <!--
        18	GeoLocation	0-n	"GeoLocationPoint|geoLocationBox|geoLocationPlace"
        -->
        <xsl:if test="property[@key='geoLocationPoint' or @key='geoLocationBox' or @key='geoLocationPlace']">
            <geoLocations>
                <xsl:apply-templates select="property[@key='geoLocationPoint' or @key='geoLocationBox' or @key='geoLocationPlace']" mode="geoLocation"/>
            </geoLocations>
        </xsl:if>

        <!--
        19	Funder	0-n	"funderName..."
        -->
        <xsl:if test="property[@key='funderName']">
            <fundingReferences>
                <xsl:apply-templates select="property[@key='funderName']" mode="funder"/>
            </fundingReferences>
        </xsl:if>
    </xsl:template>



    <xsl:template match="property" mode="identifier">
        <xsl:if test="text()">
            <identifier identifierType="DOI">
                <xsl:value-of select="text()"/>
            </identifier>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="creator">
        <xsl:if test="text()">
            <creator>
                <creatorName>
                    <xsl:value-of select="text()"/>
                </creatorName>
                <!-- optional nameIdentifier attribute -->
                <xsl:apply-templates select="attribute[starts-with(@key,'nameIdentifier')]" mode="nameIdentifier"/>
                <!-- optional affiliation attribute -->
                <xsl:apply-templates select="attribute[@key='affiliation']" mode="affiliation"/>
            </creator>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="title">
        <xsl:if test="text()">
            <title>
                <xsl:if test="@attr1">
                    <xsl:attribute name="titleType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </title>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="publisher">
        <xsl:if test="text()">
            <publisher>
                <xsl:value-of select="text()"/>
            </publisher>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="publicationYear">
        <xsl:if test="text()">
            <publicationYear>
                <xsl:value-of select="text()"/>
            </publicationYear>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="subject">
        <xsl:if test="text()">
            <subject>
                <xsl:if test="@attr1">
                    <xsl:attribute name="subjectScheme">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </subject>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="contributor">
        <xsl:if test="text()">
            <contributor>
                <xsl:if test="@attr1">
                    <xsl:attribute name="contributorType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <contributorName>
                    <xsl:value-of select="text()"/>
                </contributorName>
                <!-- optional nameIdentifier attribute -->
                <xsl:apply-templates select="attribute[starts-with(@key,'nameIdentifier')]" mode="nameIdentifier"/>
                <!-- optional affiliation attribute -->
                <xsl:apply-templates select="attribute[@key='affiliation']" mode="affiliation"/>
            </contributor>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="date">
        <xsl:if test="text()">
            <date>
                <xsl:if test="@attr1">
                    <xsl:attribute name="dateType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </date>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="language">
        <xsl:if test="text()">
            <language>
                <xsl:value-of select="text()"/>
            </language>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="resourceType">
        <xsl:if test="@attr1">
            <resourceType>
                <xsl:attribute name="resourceTypeGeneral">
                    <xsl:value-of select="@attr1"/>
                </xsl:attribute>
                <xsl:value-of select="text()"/>
            </resourceType>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="alternateIdentifier">
        <xsl:if test="text()">
            <alternateIdentifier>
                <xsl:if test="@attr1">
                    <xsl:attribute name="alternateIdentifierType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </alternateIdentifier>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="relatedIdentifier">
        <xsl:if test="text()">
            <relatedIdentifier>
                <xsl:if test="@attr1">
                    <xsl:attribute name="relationType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="@attr2">
                    <xsl:attribute name="relatedIdentifierType">
                        <xsl:value-of select="@attr2"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </relatedIdentifier>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="size">
        <xsl:if test="text()">
            <size>
                <xsl:value-of select="text()"/>
            </size>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="format">
        <xsl:if test="text()">
            <format>
                <xsl:value-of select="text()"/>
            </format>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="version">
        <xsl:if test="text()">
            <version>
                <xsl:value-of select="text()"/>
            </version>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="rights">
        <xsl:if test="text()">
            <rights>
                <!-- optional rightsURI attribute -->
                <xsl:apply-templates select="attribute[@key='rightsURI']" mode="rightsURI"/>
                <xsl:value-of select="text()"/>
            </rights>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="description">
        <xsl:if test="text()">
            <description>
                <xsl:if test="@attr1">
                    <xsl:attribute name="descriptionType">
                        <xsl:value-of select="@attr1"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="text()"/>
            </description>
        </xsl:if>
    </xsl:template>

    <xsl:template match="property" mode="geoLocation">
        <xsl:if test="text()">
            <geoLocation>
                <xsl:element name="{@key}">
                    <xsl:choose>
                        <xsl:when test="@key = 'geoLocationPoint' or @key = 'geoLocationBox'">
                            <xsl:variable name="tokens">
                                <xsl:call-template name="split">
                                    <xsl:with-param name="string" select="text()"/>
                                    <xsl:with-param name="pattern" select="' '" />
                                </xsl:call-template>
                            </xsl:variable>
                            <xsl:variable name="nodes" select="exsl:node-set($tokens)/token"/>
                            <xsl:choose>
                                <xsl:when test="@key = 'geoLocationPoint'">
                                    <pointLatitude>
                                        <xsl:value-of select="$nodes[1]/text()"/>
                                    </pointLatitude>
                                    <pointLongitude>
                                        <xsl:value-of select="$nodes[2]/text()"/>
                                    </pointLongitude>
                                </xsl:when>
                                <xsl:when test="@key = 'geoLocationBox'">
                                    <southBoundLatitude>
                                        <xsl:value-of select="$nodes[1]/text()"/>
                                    </southBoundLatitude>
                                    <westBoundLongitude>
                                        <xsl:value-of select="$nodes[2]/text()"/>
                                    </westBoundLongitude>
                                    <northBoundLatitude>
                                        <xsl:value-of select="$nodes[3]/text()"/>
                                    </northBoundLatitude>
                                    <eastBoundLongitude>
                                        <xsl:value-of select="$nodes[4]/text()"/>
                                    </eastBoundLongitude>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="text()"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </geoLocation>
        </xsl:if>
    </xsl:template>

    <!-- fundingReference (4.0) -->
    <xsl:template match="property" mode="funder">
        <xsl:if test="text()">
            <fundingReference>
                <funderName>
                    <xsl:value-of select="text()"/>
                </funderName>
                <!-- optional awardNumber attribute -->
                <xsl:apply-templates select="attribute[@key='awardNumber']" mode="awardNumber"/>
                <!-- optional awardTitle attribute -->
                <xsl:apply-templates select="attribute[@key='awardTitle']" mode="awardTitle"/>
            </fundingReference>
        </xsl:if>
    </xsl:template>

    <!-- attributes -->

    <xsl:template match="attribute" mode="rightsURI">
        <xsl:if test="text()">
            <xsl:attribute name="rightsURI">
                <xsl:value-of select="text()"/>
            </xsl:attribute>
        </xsl:if>
    </xsl:template>

    <xsl:template match="attribute" mode="nameIdentifier">
        <xsl:if test="text()">
            <nameIdentifier>
                <xsl:attribute name="nameIdentifierScheme">
                    <xsl:value-of select="substring-after(@key,':')"/>
                </xsl:attribute>
                <xsl:value-of select="text()"/>
            </nameIdentifier>
        </xsl:if>
    </xsl:template>

    <xsl:template match="attribute" mode="affiliation">
        <xsl:if test="text()">
            <affiliation>
                <xsl:value-of select="text()"/>
            </affiliation>
        </xsl:if>
    </xsl:template>

    <!-- fundingReference attributes (4.0) -->
    <xsl:template match="attribute" mode="awardNumber">
        <xsl:if test="text()">
            <awardNumber>
                <xsl:value-of select="text()"/>
            </awardNumber>
        </xsl:if>
    </xsl:template>

    <xsl:template match="attribute" mode="awardTitle">
        <xsl:if test="text()">
            <awardTitle>
                <xsl:value-of select="text()"/>
            </awardTitle>
        </xsl:if>
    </xsl:template>



    <!-- split pattern -->
    <xsl:template name="split">
        <xsl:param name="string" select="''" />
        <xsl:param name="pattern" select="' '" />
        <xsl:choose>
            <xsl:when test="not($string)" />
            <xsl:when test="not($pattern)">
                <xsl:call-template name="_split-characters">
                    <xsl:with-param name="string" select="$string" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="_split-pattern">
                    <xsl:with-param name="string" select="$string" />
                    <xsl:with-param name="pattern" select="$pattern" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="_split-characters">
        <xsl:param name="string" />
        <xsl:if test="$string">
            <token>
                <xsl:value-of select="substring($string, 1, 1)" />
            </token>
            <xsl:call-template name="_split-characters">
                <xsl:with-param name="string" select="substring($string, 2)" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="_split-pattern">
        <xsl:param name="string" />
        <xsl:param name="pattern" />
        <xsl:choose>
            <xsl:when test="contains($string, $pattern)">
                <xsl:if test="not(starts-with($string, $pattern))">
                    <token>
                        <xsl:value-of select="substring-before($string, $pattern)" />
                    </token>
                </xsl:if>
                <xsl:call-template name="_split-pattern">
                    <xsl:with-param name="string" select="substring-after($string, $pattern)" />
                    <xsl:with-param name="pattern" select="$pattern" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <token>
                    <xsl:value-of select="$string" />
                </token>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
