<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:h="http://www.w3.org/1999/xhtml"
                xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd">

    <xsl:template name="custom-style">
        <xsl:param name="page_title"/>
        <xsl:param name="embedded" select="''"/>

        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />


                <title>
                    <xsl:value-of select="$page_title" />
                </title>

                <!-- Bootstrap core CSS -->
                <link href="{$www_root}bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

                <!-- Custom styles for this template -->
                <xsl:choose>
                    <xsl:when test="$embedded = ''">
                        <link href="{$www_root}css/DOI-template.css" rel="stylesheet"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <link href="{$www_root}css/DOI-template-embed.css" rel="stylesheet"/>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:text disable-output-escaping="yes">
<![CDATA[
                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->
]]></xsl:text>
            </head>
            <body>
                <xsl:if test="$embedded = ''">
                    <nav class="navbar navbar-inverse navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar">-</span>
                                    <span class="icon-bar">-</span>
                                    <span class="icon-bar">-</span>
                                </button>
                                <a class="navbar-brand" href="{$www_root}index.html">OSUG DOI</a>
                            </div>
                            <div id="navbar" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Links <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{$www_root}xsd/4.1/metadata.xsd">XSD Schema 4.1</a>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li class="dropdown-header">Datacite links</li>
                                            <li>
                                                <a href="http://schema.datacite.org/">DataCite Metadata Schema</a>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <a href="http://search.datacite.org/">DataCite Search</a>
                                            </li>
                                            <li>
                                                <a href="http://citation.crosscite.org/">DataCite Citation formatter</a>
                                            </li>
                                            <li role="separator" class="divider"></li>
                                            <li>
                                                <a href="http://stats.datacite.org/">DataCite Statistics</a>
                                            </li>
                                            <li>
                                                <a href="http://mds.datacite.org/">Datacite Metadata Store</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{$www_root}about.html">About</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </xsl:if>

                <div class="container">
                    <xsl:call-template name="custom-container" />
                </div>

                <xsl:if test="$embedded = ''">
                    <footer class="footer">
                        <div class="container">
                            <p class="text-muted">(c) 2019 - <a href="{$www_root}index.html">OSUG DOI</a></p>
                        </div>
                    </footer>
                </xsl:if>

                <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
                <xsl:text disable-output-escaping="yes"><![CDATA[
                <script src="]]></xsl:text>
                <xsl:value-of select="$www_root"/>
                <xsl:text disable-output-escaping="yes"><![CDATA[js/jquery-1.12.4.min.js"></script>
]]></xsl:text>
                <!-- Include all compiled plugins (below), or include individual files as needed -->
                <xsl:text disable-output-escaping="yes"><![CDATA[
                <script src="]]></xsl:text>
                <xsl:value-of select="$www_root"/>
                <xsl:text disable-output-escaping="yes"><![CDATA[bootstrap/js/bootstrap.min.js"></script>
]]></xsl:text>
                <!-- enable popover (js) -->
                <script>
                    $(function () { $('[data-toggle="popover"]').popover() })
                </script>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
