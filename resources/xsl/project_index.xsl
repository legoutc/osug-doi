<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd">

    <xsl:output method="xml" indent="no" omit-xml-declaration="no" media-type="text/html" encoding="utf-8"
                doctype-public="-//W3C//DTD XHTML 1.1//EN"
                doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

    <!-- global parameters -->
    <xsl:param name="www_root" select="''"/>
    <xsl:param name="date" select="''"/>
    <xsl:param name="parent" select="''"/>
    <xsl:param name="current" select="''"/>

    <xsl:variable name="is_folder" select="$parent = ''"/>
    <xsl:variable name="is_staging" select="$current = 'staging'"/>

    <xsl:include href="./bootstrap_style.xsl"/>




    <!-- base template -->
    <xsl:template match="/">
        <xsl:call-template name="custom-style">
            <xsl:with-param name="page_title">
                OSUG DOI -
                <xsl:if test="$parent != ''">
                    <xsl:value-of select="$parent"/> - </xsl:if>
                <xsl:value-of select="$current"/>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>


    <!-- container template -->
    <!-- same for index / report -->
    <xsl:template name="custom-container">
        <ol class="breadcrumb">
            <li>
                <a href="{$www_root}index.html">OSUG DOI</a>
            </li>
            <xsl:if test="$parent">
                <li>
                    <a href="{$www_root}{$parent}/index.html">
                        <xsl:value-of select="$parent"/>
                    </a>
                </li>
            </xsl:if>
            <xsl:if test="$current">
                <li class="active">
                    <xsl:value-of select="$current"/>
                </li>
            </xsl:if>
        </ol>

        <div class="doi-template">
            <xsl:apply-templates />

            <div class="text-right small">
                <xsl:value-of select="$date"/>
            </div>
        </div>
    </xsl:template>


    <!-- index -->
    <xsl:template match="index">
        <xsl:choose>
            <xsl:when test="count(./item) != 0">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <xsl:choose>
                                    <xsl:when test="$is_folder">
                                        <th>Project</th>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <th>DOI</th>
                                        <th>Description</th>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <th>Updated</th>
                                <xsl:if test="$is_folder">
                                    <th>Count</th>
                                </xsl:if>
                                <xsl:if test="$is_staging">
                                    <th></th>
                                </xsl:if>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:apply-templates select="./item"/>
                        </tbody>
                    </table>
                </div>
            </xsl:when>
            <xsl:otherwise>
                That's All Folks !
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="item">
        <tr>
            <td>
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="./name/text()"/>
                        <xsl:choose>
                            <xsl:when test="$is_folder">/index.html</xsl:when>
                            <xsl:otherwise>.html</xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:value-of select="./name/text()"/>
                </a>
            </td>
            <xsl:if test="not($is_folder)">
                <td>
                    <xsl:value-of select="./description/text()"/>
                </td>
            </xsl:if>
            <td>
                <xsl:value-of select="./update/text()"/>
            </td>
            <xsl:if test="$is_folder">
                <td>
                    <xsl:value-of select="./count/text()"/>
                </td>
            </xsl:if>
            <xsl:if test="$is_staging">
                <td>
                    <a href="{./name/text()}/report.html" class="btn btn-primary btn-xs">Report</a>
                    <a href="{./name/text()}/error.html" class="btn btn-primary btn-xs">Error</a>
                </td>
            </xsl:if>
        </tr>
    </xsl:template>

</xsl:stylesheet>
