<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://datacite.org/schema/kernel-3"
                exclude-result-prefixes="d"
                xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd
                                    http://datacite.org/schema/kernel-3 http://schema.datacite.org/meta/kernel-3/metadata.xsd">

    <xsl:output method="xml" indent="no" omit-xml-declaration="no" media-type="text/html" encoding="utf-8"
                doctype-public="-//W3C//DTD XHTML 1.1//EN"
                doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

    <!-- global parameters -->
    <xsl:param name="metadataFile" select="''"/>
    <xsl:param name="www_root" select="''"/>
    <xsl:param name="date" select="''"/>

    <!-- page parameters -->
    <!-- optional embedded mode (full, header, meta) -->
    <xsl:param name="embedded" select="''"/>

    <!-- specific parameters -->
    <xsl:param name="data_access_html" select="''"/>
    <xsl:param name="data_access_url" select="''"/>

    <!-- optional external landing page -->
    <xsl:param name="landing_page_url" select="''"/>

    <xsl:include href="../bootstrap_style.xsl"/>




    <!-- global variables -->
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyzäöü'" />
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'" />

    <xsl:variable name="doi_resolver">http://dx.doi.org/</xsl:variable>
    <xsl:variable name="viaf_resolver">http://viaf.org/viaf/</xsl:variable>
    <xsl:variable name="isni_resolver">http://isni.oclc.nl/DB=1.2/SET=4/TTL=18/CMD?ACT=SRCHA&amp;IKT=8006&amp;SRT=&amp;TRM=</xsl:variable>
    <xsl:variable name="orcid_resolver">http://orcid.org/</xsl:variable>
    <xsl:variable name="authorclaim_resolver">http://authorclaim.org/profile/</xsl:variable>
    <xsl:variable name="researcherid_resolver">http://www.researcherid.com/rid/</xsl:variable>




    <!-- base template -->
    <xsl:template match="/">
        <xsl:call-template name="custom-style">
            <xsl:with-param name="page_title">DOI Metadata for <xsl:call-template name="get_identifier" /></xsl:with-param>
            <xsl:with-param name="embedded" select="$embedded"/>
        </xsl:call-template>
    </xsl:template>


    <!-- container template -->
    <xsl:template name="custom-container">
        <xsl:if test="$embedded = '' or $embedded = 'full' or $embedded = 'header'">
            <div class="jumbotron">
                <xsl:call-template name="header" />
            </div>

            <xsl:call-template name="data-access" />
        </xsl:if>

        <xsl:if test="$embedded = '' or $embedded = 'full' or $embedded = 'meta'">
            <div class="doi-template">
                <xsl:apply-templates />

                <xsl:if test="$date">
                    <div class="text-right small">
                        <xsl:value-of select="$date"/>
                    </div>
                </xsl:if>
            </div>
        </xsl:if>
    </xsl:template>


    <!-- DOI page header template -->
    <xsl:template name="header">
        <h3>DOI Metadata for <xsl:call-template name="get_identifier" /></h3>

        <xsl:call-template name="citation" />

        <xsl:if test="$embedded = ''">
            <p>
                <xsl:if test="$landing_page_url != ''">
                    <a class="label label-primary" href="{$landing_page_url}" target="_blank" role="button">External landing page</a>
                </xsl:if>
                <xsl:if test="$metadataFile != ''">
                    <a class="label label-primary" href="xml/{$metadataFile}" target="_blank" role="button">XML</a>
                </xsl:if>
            </p>
        </xsl:if>
    </xsl:template>




    <!-- data access template -->
    <xsl:template name="data-access">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">Data access instructions</h2>
            </div>
            <div class="panel-body">
                <xsl:choose>
                    <xsl:when test="$data_access_html = ''">Missing instructions to access data or services !</xsl:when>
                    <xsl:otherwise>
                        <!-- load fragment -->
                        <xsl:variable name="fragment" select="document($data_access_html)"/>

                        <xsl:choose>
                            <xsl:when test="count($fragment/xhtml:html/xhtml:body/child::node()) = 0">Missing instructions to access data or services !</xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="$fragment/xhtml:html/xhtml:body/child::node()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="$data_access_url">
                    <p>
                        <a href="{$data_access_url}" target="_blank">
                            <xsl:value-of select="$data_access_url"/>
                        </a>
                    </p>
                </xsl:if>
            </div>
        </div>
    </xsl:template>




    <!-- DOI xml root = resource -->
    <xsl:template match="d:resource">
        <table class="table table-condensed">
            <tbody>
                <!-- main information -->
                <xsl:apply-templates select="./*[local-name() = 'identifier'
                                                or local-name() = 'creators'
                                                or local-name() = 'titles'
                                                or local-name() = 'publisher'
                                                or local-name() = 'publicationYear']"/>

                <tr>
                    <th colspan="2">
                        <br/>
                    </th>
                </tr>

                <!-- custom metadata order -->
                <xsl:apply-templates select="./d:descriptions"/>
                <xsl:apply-templates select="./d:rightsList"/>
                <xsl:apply-templates select="./d:resourceType"/>
                <xsl:apply-templates select="./d:subjects"/>
                <xsl:apply-templates select="./d:geoLocations"/>
                <xsl:apply-templates select="./d:dates"/>


                <!-- remaining meta data -->
                <xsl:apply-templates select="./*[not(local-name() = 'identifier'
                                                or local-name() = 'creators'
                                                or local-name() = 'titles'
                                                or local-name() = 'publisher'
                                                or local-name() = 'publicationYear'
                                                or local-name() = 'descriptions'
                                                or local-name() = 'rightsList'
                                                or local-name() = 'resourceType'
                                                or local-name() = 'subjects'
                                                or local-name() = 'geoLocations'
                                                or local-name() = 'dates'
                                            )]"/>
            </tbody>
        </table>
    </xsl:template>


    <!-- 1. Identifier -->
    <xsl:template match="d:identifier">
        <tr class="active bold">
            <th class="flabel">
                <xsl:value-of select="@identifierType" />
                identifier
            </th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 2. Creator(s) -->
    <xsl:template match="d:creators">
        <tr class="active bold">
            <th class="flabel">Creators</th>
            <td>
                <xsl:apply-templates />
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:creators/d:creator">
        <xsl:call-template name="person-link">
            <xsl:with-param name="text" select="d:creatorName"/>
        </xsl:call-template>
        <xsl:if test="./d:affiliation">
            <xsl:text> (</xsl:text>
            <xsl:value-of select="./d:affiliation/text()" />
            <xsl:text>)</xsl:text>
        </xsl:if>
        <br/>
    </xsl:template>


    <!-- 3. Title(s) -->
    <xsl:template match="d:titles">
        <xsl:if test="d:title[not(@titleType)]">
            <xsl:apply-templates select="d:title[not(@titleType)]"/>
        </xsl:if>
        <xsl:if test="d:title[@titleType]">
            <xsl:apply-templates select="d:title[@titleType]"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="d:title">
        <xsl:variable name="row_css">
            <xsl:choose>
                <xsl:when test="not(@titleType)">flabel</xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <tr class="active bold">
            <th class="{$row_css}">
                <xsl:choose>
                    <xsl:when test="@titleType">
                        <xsl:value-of select="@titleType" />
                    </xsl:when>
                    <xsl:otherwise>Title</xsl:otherwise>
                </xsl:choose>
            </th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 4. Publisher -->
    <xsl:template match="d:publisher">
        <tr class="active bold">
            <th class="flabel">Publisher</th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 5. PublicationYear -->
    <xsl:template match="d:publicationYear">
        <tr class="active bold">
            <th class="flabel">Year of publication</th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 6. Subject(s) -->
    <xsl:template match="d:subjects">
        <tr>
            <th class="flabel">Subjects</th>
            <td>
                <ul>
                    <xsl:apply-templates />
                </ul>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:subjects/d:subject">
        <li>
            <!--
            <xsl:if test="@subjectScheme">
                <xsl:value-of select="@subjectScheme" />:
            </xsl:if>
            -->
            <xsl:value-of select="." />
        </li>
    </xsl:template>


    <!-- 7. Contributor(s) -->
    <xsl:template match="d:contributors">
        <tr>
            <th class="flabel" colspan="2">Contributors</th>
        </tr>

        <!-- custom metadata order -->
        <xsl:apply-templates select="./d:contributor[@contributorType = 'ContactPerson']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'ProjectLeader']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'WorkPackageLeader']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'ProjectMember']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'DataCollector']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'Other']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'RelatedPerson']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'DataManager']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'Funder']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'Sponsor']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'ResearchGroup']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'Distributor']"/>
        <xsl:apply-templates select="./d:contributor[@contributorType = 'HostingInstitution']"/>

        <!-- remaining meta data -->
        <xsl:apply-templates select="./d:contributor[@contributorType != 'ContactPerson'
                                        and @contributorType != 'ProjectLeader'
                                        and @contributorType != 'WorkPackageLeader'
                                        and @contributorType != 'ProjectMember'
                                        and @contributorType != 'DataCollector'
                                        and @contributorType != 'Other'
                                        and @contributorType != 'RelatedPerson'
                                        and @contributorType != 'DataManager'
                                        and @contributorType != 'Funder'
                                        and @contributorType != 'Sponsor'
                                        and @contributorType != 'ResearchGroup'
                                        and @contributorType != 'Distributor'
                                        and @contributorType != 'HostingInstitution'
                                    ]"/>
    </xsl:template>

    <xsl:template match="d:contributors/d:contributor">
        <tr class="no-border">
            <th>
                <xsl:value-of select="@contributorType" />
            </th>
            <td>
                <xsl:call-template name="person-link">
                    <xsl:with-param name="text" select="d:contributorName"/>
                </xsl:call-template>
                <xsl:if test="./d:affiliation">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="./d:affiliation/text()" />
                    <xsl:text>)</xsl:text>
                </xsl:if>
            </td>
        </tr>
    </xsl:template>


    <!-- 8. Date(s) -->
    <xsl:template match="d:dates">
        <tr>
            <th class="flabel" colspan="2">Dates</th>
        </tr>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="d:dates/d:date">
        <tr class="no-border">
            <th>
                <xsl:value-of select="@dateType" />
            </th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 9. Language -->
    <xsl:template match="d:language">
        <tr>
            <th class="flabel">Primary language</th>
            <td>
                <xsl:choose>
                    <xsl:when test="text() = 'en'">english</xsl:when>
                    <xsl:when test="text() = 'fr'">french</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="text()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>


    <!-- 10. Resource type -->
    <xsl:template match="d:resourceType">
        <tr>
            <th class="flabel">Resource type</th>
            <td>
                <xsl:value-of select="@resourceTypeGeneral" />
                <xsl:if test=". != ''"> / <xsl:value-of select="." /></xsl:if>
            </td>
        </tr>
    </xsl:template>


    <!-- 11. Alternate identifier(s) -->
    <xsl:template match="d:alternateIdentifiers">
        <tr>
            <th class="flabel" colspan="2">Alternate identifiers</th>
        </tr>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="d:alternateIdentifiers/d:alternateIdentifier">
        <tr class="no-border">
            <th>
                <xsl:value-of select="@alternateIdentifierType" />
            </th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 12. Related identifier(s) -->
    <xsl:template match="d:relatedIdentifiers">
        <tr>
            <th class="flabel" colspan="2">Related identifiers</th>
        </tr>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="d:relatedIdentifiers/d:relatedIdentifier">
        <tr class="no-border">
            <th>
                <xsl:value-of select="@relationType" />
            </th>
            <td>
                <xsl:value-of select="@relatedIdentifierType" />
                <xsl:text>: </xsl:text>
                <xsl:choose>
                    <xsl:when test="translate(@relatedIdentifierType, $lowercase, $uppercase) = 'URL'">
                        <a target="_blank">
                            <xsl:attribute name="href">
                                <xsl:value-of select="." />
                            </xsl:attribute>
                            <xsl:value-of select="." />
                        </a>
                    </xsl:when>
                    <xsl:when test="translate(@relatedIdentifierType, $lowercase, $uppercase) = 'DOI'">
                        <a target="_blank">
                            <xsl:attribute name="href">
                                <xsl:value-of select="concat($doi_resolver, .)" />
                            </xsl:attribute>
                            <xsl:value-of select="." />
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="." />
                    </xsl:otherwise>
                </xsl:choose>
            </td>
        </tr>
    </xsl:template>


    <!-- 13. Size -->
    <xsl:template match="d:sizes">
        <tr>
            <th class="flabel">Sizes</th>
            <td>
                <ul>
                    <xsl:apply-templates />
                </ul>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:sizes/d:size">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>


    <!-- 14. Format(s) -->
    <xsl:template match="d:formats">
        <tr>
            <th class="flabel">Formats</th>
            <td>
                <ul>
                    <xsl:apply-templates />
                </ul>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:formats/d:format">
        <li>
            <xsl:value-of select="." />
        </li>
    </xsl:template>


    <!-- 15. Version -->
    <xsl:template match="d:version">
        <tr>
            <th class="flabel">Version</th>
            <td>
                <xsl:value-of select="." />
            </td>
        </tr>
    </xsl:template>


    <!-- 16. Rights -->
    <xsl:template match="d:rightsList">
        <tr>
            <th class="flabel">Rights</th>
            <td class="text-justify">
                <xsl:apply-templates />
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:rightsList/d:rights">
        <!-- rightsURI -->
        <xsl:choose>
            <xsl:when test="@rightsURI">
                <a href="{@rightsURI}" target="_blank">
                    <xsl:call-template name="add-line-breaks">
                        <xsl:with-param name="text" select="."/>
                    </xsl:call-template>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="add-line-breaks">
                    <xsl:with-param name="text" select="."/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
        <br/>
    </xsl:template>


    <!-- 17. Description(s) -->
    <xsl:template match="d:descriptions">
        <tr>
            <th class="flabel" colspan="2">Descriptions</th>
        </tr>
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="d:descriptions/d:description">
        <tr class="no-border">
            <th>
                <xsl:value-of select="@descriptionType" />
            </th>
            <td class="text-justify">
                <xsl:call-template name="add-line-breaks">
                    <xsl:with-param name="text" select="."/>
                </xsl:call-template>
            </td>
        </tr>
    </xsl:template>


    <!-- 18. GeoLocation(s) -->
    <xsl:template match="d:geoLocations">
        <tr>
            <th class="flabel">GeoLocations</th>
            <td>
                <ul>
                    <xsl:apply-templates />
                </ul>
            </td>
        </tr>
    </xsl:template>

    <xsl:template match="d:geoLocations/d:geoLocation">
        <li>
            <!-- TODO: tokenize and display properly point and box -->
            <xsl:choose>
                <xsl:when test="d:geoLocationPlace">
                    <xsl:value-of select="d:geoLocationPlace" />
                </xsl:when>
                <xsl:when test="d:geoLocationPoint">
                    <xsl:value-of select="d:geoLocationPoint" />
                    <xsl:text> (N. E.)</xsl:text>
                </xsl:when>
                <xsl:when test="d:geoLocationBox">
                    <xsl:value-of select="d:geoLocationBox" />
                    <xsl:text> (S. W. N. E.)</xsl:text>
                </xsl:when>
            </xsl:choose>
        </li>
    </xsl:template>




    <!-- utility templates -->

    <xsl:template name="get_identifier">
        <xsl:value-of select="d:resource/d:identifier" />
    </xsl:template>

    <xsl:template name="citation">
        <p class="text-justify">
            <b>Suggested citation:</b>
            <br/>
            <xsl:for-each select="d:resource/d:creators/d:creator">
                <xsl:value-of select="d:creatorName" />
                <xsl:if test="not(position() = last())">; </xsl:if>
            </xsl:for-each>
            (<xsl:value-of select="d:resource/d:publicationYear"/>):
            <xsl:for-each select="d:resource/d:titles/d:title[not(@titleType)]">
                <xsl:value-of select="."/>. </xsl:for-each>
            <xsl:value-of select="d:resource/d:publisher"/>.
            <a target="_blank">
                <xsl:attribute name="href">
                    <xsl:value-of select="concat($doi_resolver, d:resource/d:identifier)" />
                </xsl:attribute>
                doi:<xsl:value-of select="d:resource/d:identifier" />
            </a>
        </p>
    </xsl:template>

    <xsl:template name="add-line-breaks">
        <xsl:param name="string" select="." />
        <xsl:choose>
            <xsl:when test="contains($string, '&#xA;')">
                <p>
                    <xsl:value-of select="substring-before($string, '&#xA;')" />
                </p>
                <xsl:call-template name="add-line-breaks">
                    <xsl:with-param name="string"
                                    select="substring-after($string, '&#xA;')" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$string" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="person-link">
        <xsl:param name="text" select="." />
        <xsl:choose>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'RESEARCHERID'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($researcherid_resolver,d:nameIdentifier)" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'AUTHORCLAIM'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($authorclaim_resolver,d:nameIdentifier)" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'ORCID'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($orcid_resolver,d:nameIdentifier)" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'ISNI'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($isni_resolver,d:nameIdentifier)" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'VIAF'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat($viaf_resolver,d:nameIdentifier)" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:when test="translate(d:nameIdentifier/@nameIdentifierScheme, $lowercase, $uppercase) = 'URL'">
                <a target="_blank">
                    <xsl:attribute name="href">
                        <xsl:value-of select="d:nameIdentifier" />
                    </xsl:attribute>
                    <xsl:value-of select="$text" />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
