<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:d="http://datacite.org/schema/kernel-3"
                exclude-result-prefixes="d"
                xsi:schemaLocation="http://datacite.org/schema/kernel-3 http://schema.datacite.org/meta/kernel-3/metadata.xsd"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl">

    <xsl:output method="text" indent="no" omit-xml-declaration="yes" encoding="utf-8"/>

    <xsl:variable name="REL_PATH" select="'../../xsd/3.1/'"/>

    <xsl:variable name="included" select="//xs:include/@schemaLocation"/>




    <!-- xsd root = resource -->
    <xsl:template match="xs:schema">

        <xsl:for-each select="$included">
            <xsl:message>xsd:include: <xsl:value-of select="."/></xsl:message>
        </xsl:for-each>

        <!-- get resource/* elements -->
        <xsl:for-each select="./xs:element/xs:complexType/xs:all/xs:element">
            <xsl:text># </xsl:text>
            <xsl:value-of select="position()"/>
            <xsl:text> [</xsl:text>
            <xsl:value-of select="@name"/>
            <xsl:text>]&#13;</xsl:text>

            <xsl:call-template name="element">
                <xsl:with-param name="parent" select="''"/>
            </xsl:call-template>
            <xsl:text>&#13;</xsl:text>
        </xsl:for-each>
    </xsl:template>




    <xsl:template name="element">
        <xsl:param name="parent"/>

        <xsl:param name="node" select="."/>
        <xsl:param name="elementName" select="@name"/>

        <!-- list of values are within an element whose name ends with 's' like titles : -->
        <xsl:variable name="isList" select="$elementName = 'rightsList' or ($elementName != 'rights' and substring($elementName, string-length($elementName)) = 's')"/>

        <xsl:variable name="classNode" select="./xs:complexType/xs:sequence" />

        <xsl:variable name="attrType" select="@type"/>

        <!-- type values -->
        <xsl:variable name="values">
            <xsl:choose>
                <!-- specific values -->
                <xsl:when test="$elementName = 'identifierType'">
                    <values>
                        <value>DOI</value>
                    </values>
                </xsl:when>
                <xsl:when test="$elementName = 'subjectScheme'">
                    <values>
                        <value>main</value>
                        <value>var</value>
                    </values>
                </xsl:when>
                <xsl:when test="$attrType">
                    <xsl:call-template name="typeValues">
                        <xsl:with-param name="attrType" select="$attrType"/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="local-name() = 'attribute' and string-length($values) != 0">
                <xsl:variable name="nodes" select="exsl:node-set($values)/values/value"/>
                <xsl:for-each select="$nodes">
                    <xsl:variable name="path">
                        <!-- ignore element name -->
                        <xsl:value-of select="concat($parent, ':', text())"/>
                    </xsl:variable>

                    <xsl:message>PATH VALUE: <xsl:value-of select="$path"/></xsl:message>

                    <xsl:call-template name="property">
                        <xsl:with-param name="key" select="$path"/>
                        <xsl:with-param name="desc" select="concat($elementName, ' = ', text())"/>
                        <xsl:with-param name="prefix" select="''"/>
                    </xsl:call-template>

                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="path">
                    <xsl:choose>
                        <xsl:when test="$elementName = 'creator'">
                            <xsl:value-of select="'creatorName'"/>
                        </xsl:when>
                        <xsl:when test="$elementName = 'contributor'">
                            <xsl:value-of select="'contributorName'"/>
                        </xsl:when>
                        <xsl:when test="$elementName = 'fundingReference'">
                            <xsl:value-of select="'funderName'"/>
                        </xsl:when>
                        <xsl:when test="local-name() = 'element' and contains($elementName, $parent)">
                            <xsl:value-of select="$elementName"/>
                        </xsl:when>
                        <xsl:when test="string-length($parent) != 0">
                            <xsl:value-of select="concat($parent, '.', $elementName)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$elementName"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:variable name="child_path">
                    <xsl:choose>
                        <xsl:when test="$isList">
                            <xsl:value-of select="$parent"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$path"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:message>PATH: <xsl:value-of select="$path"/></xsl:message>
                <xsl:message>values: <xsl:value-of select="$values"/></xsl:message>

                <xsl:call-template name="property">
                    <xsl:with-param name="key" select="$path"/>
                    <xsl:with-param name="desc">
                        <xsl:choose>
                            <xsl:when test="string-length($values) != 0">
                                <xsl:value-of select="$values"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="xs:annotation/xs:documentation/text()"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                    <xsl:with-param name="prefix">
                        <xsl:choose>
                            <xsl:when test="$isList or $classNode">
                                <xsl:value-of select="'# '"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="''"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:with-param>
                </xsl:call-template>

                <!-- recursion: child attribute -->
                <xsl:for-each select="./xs:complexType/xs:attribute[@name] | ./xs:complexType/xs:simpleContent//xs:attribute[@name]">
                    <!-- ignore schemeURI, rightsURI, relatedMetadataScheme, schemeType -->
                    <xsl:if test="not(contains(@name, 'URI') or (contains(@name, 'cheme') and @name != 'subjectScheme') )">
                        <xsl:if test="@name = 'relatedIdentifierType'">
                            <xsl:text># Only to list all possible values (but should be composed with relatedIdentifier:&lt;relationType&gt; { &#13;</xsl:text>
                        </xsl:if>
                        <xsl:call-template name="element">
                            <xsl:with-param name="parent" select="$child_path"/>
                        </xsl:call-template>
                        <xsl:if test="@name = 'relatedIdentifierType'">
                            <xsl:text># } &#13;</xsl:text>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
                <!-- recursion: child element -->
                <xsl:for-each select="./xs:complexType/xs:sequence/xs:element[@name != 'contributorName']">
                    <xsl:choose>
                        <xsl:when test="@name != 'nameIdentifier' and @name != 'affiliation'">
                            <xsl:call-template name="element">
                                <xsl:with-param name="parent" select="$child_path"/>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text># optional </xsl:text>
                            <xsl:value-of select="@name"/>
                            <xsl:if test="@name = 'nameIdentifier'">:ORCID</xsl:if>
                            <xsl:text>&#13;</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>




    <xsl:template name="property">
        <xsl:param name="key"/>
        <xsl:param name="desc"/>
        <xsl:param name="prefix" select="''"/>

        <xsl:value-of select="$prefix"/>
        <xsl:value-of select="$key"/>
        <xsl:text>&#09;&#09;&#09;&#09;</xsl:text>
        <xsl:if test="string-length($desc) != 0">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="translate($desc, '&#10;&#13;', '')"/>
            <xsl:text>]</xsl:text>
        </xsl:if>
        <xsl:text>&#13;</xsl:text>
    </xsl:template>


    <xsl:template name="typeValues">
        <xsl:param name="attrType"/>

        <xsl:message>type: <xsl:value-of select="$attrType"/></xsl:message>

        <xsl:variable name="resolvedType" select="//xs:simpleType[@name = $attrType]"/>

        <xsl:choose>
            <xsl:when test="$resolvedType">
                <values>
                    <xsl:for-each select="$resolvedType//xs:enumeration">
                        <value>
                            <xsl:value-of select="@value"/>
                        </value>
                    </xsl:for-each>
                </values>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$included">
                    <xsl:variable name="doc" select="document(concat($REL_PATH, .))/xs:schema"/>

                    <xsl:variable name="resolvedType2" select="$doc//xs:simpleType[@name = $attrType]"/>

                    <xsl:if test="$resolvedType2">
                        <values>
                            <xsl:for-each select="$resolvedType2//xs:enumeration">
                                <value>
                                    <xsl:value-of select="@value"/>
                                </value>
                            </xsl:for-each>
                        </values>
                    </xsl:if>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
