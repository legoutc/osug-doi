#!/bin/bash

# SpringBoot profile: dev or production
PROFILE="default" # default = h2 db
#PROFILE="prod"   # production = postgresql

JAVA_OPTS="-Xms128m -Xmx512m -Dspring.profiles.active=$PROFILE"


# force UTF-8 encoding (debian)
export JAVA_OPTS="$JAVA_OPTS -Dfile.encoding=UTF-8"

export APP_CMD="java $JAVA_OPTS -jar doimgr-trunk.jar --v=3"

#echo "JAVA_OPTS: $JAVA_OPTS"

