#!/bin/bash

# initialize environment:
source env.sh


if [ "$#" -eq  "0" ]
  then
    echo "missing project argument"
    exit 1
else
    PROJECT=$1
fi


# Process urls (data access and external landing pages) CSV to generate landing pages:
$APP_CMD --action=process-url --project=$PROJECT

if [ $? -ne 0 ]; then
  echo "java process failed"
  exit 1
fi

